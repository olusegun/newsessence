<?php
/**
 * The main template file.
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package news_essence
 */
get_header(); ?>
				
				<!-- #featured thumbnail with sidebar-1 row block-3 -->
				<div id="block-3" class="row">
				<div class="col-sm-8 col-md-8 c12" style="padding-right: 0px;">
				<?php require get_template_directory(). '/inc/newsessence-featuredpost.php'; ?>
				</div>
				<?php get_sidebar('right-1'); ?>
				</div>
				<!--end  #slider latest sidebar-1 row block-3  -->

				<!-- #exclusive content without sidebar block-4 -->
				<?php if ( get_theme_mod( 'show_exclusive_content' ) == 'yes' ) { ?>
				<div id="block-4" class="row">
				<?php require get_template_directory() . '/inc/newsessence-exclusive-content.php'; ?>
				</div>
				<?php  } ?>
				<!-- end of #exclusive block-4 -->

				<!-- #Content widget with sidebar 2 row block-5 -->
				<div id="block-5" class="row">
				<div class="block-5c col-md-8 col-sm-8" style="padding-left: 0px;">
					<?php dynamic_sidebar('widget-content-1'); ?>
				</div>
				<?php  get_sidebar('right-2'); ?>
				</div>
				<!-- end #most view row block-5 -->

				<!-- #middle advert row block-6 without sidebar -->
				<div id="block-6" class="row advert-banner">
				<?php dynamic_sidebar('middle-adsbanner'); ?>
					
				</div>
				<!-- end of #middle advert row block-6 -->

				<!-- # content widget with sidebar3 row block-7 -->
				<div id="block-7" class="row">
				<div class="block-5c col-md-8 col-sm-8" style="padding-left: 0px;">
					<?php dynamic_sidebar('widget-content-2'); ?>
				</div>
				<?php  get_sidebar('right-3'); ?>
				</div>
				<!-- end #most row block-7 -->

				<!-- #middle advert row block-8 without sidebar -->
				<div id="block-8" class="row advert-banner">
				<?php dynamic_sidebar('bottom-adsbanner'); ?>
				</div>
				<!-- end of #middle advert row block-8 -->
				<div id="main" class="row" role="main" itemprop="mainContentOfPage" itemscope="itemscope" itemtype="http://schema.org/Blog">
				<?php dynamic_sidebar('widget-content-3'); ?>
				</div>								
		</div><!-- #content -->
<?php 
get_footer();