<?php
/**
 * The header for our theme.
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package news_essence
 */

?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="theme-color" content="<?php echo get_theme_mod('mainnavbg_color'); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no, shrink-to-fit=no">
<link rel="profile" href="http://gmpg.org/xfn/11">
<meta http-equiv="x-ua-compatible" content="ie=edge">
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
<?php wp_head(); ?>
</head>
<body <?php body_class(); ?> itemscope="itemscope" itemtype="http://schema.org/WebPage">
	<header class="site-header" role="banner" itemscope="itemscope" itemtype="http://schema.org/WPHeader">
		<!-- .site-branding -->
		<nav class="navbar masthead">
  		<div class="container-fluid">
    	<div class="navbar-header">
      <button type="button" class="navbar-toggle hide" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        </button>
            <h1 id="site-logo" itemprop="headline">
                    <?php if(get_theme_mod('nf_logo')):?>
                    <a class="navbar-brand" style="padding-top: 0px !important; padding-bottom: 0px!important;" href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php bloginfo( 'name','display' ); ?>" rel="home">
                     <img class="img-responsive" src="<?php echo get_theme_mod('nf_logo'); ?>" alt="<?php bloginfo( 'name','display' ); ?>" ></a> 
                    <?php else: ?>
                    <a style="padding-top: 5px !important; padding-bottom: 0px!important;" href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a>
                    <?php
                    $description = get_bloginfo( 'description', 'display' );
                    if ( $description || is_customize_preview() ) : ?>
                        <small itemprop="description" class="site-description text-muted" style="display:none;"><?php echo $description; /* WPCS: xss ok. */ ?></small>
                    <?php
                    endif; ?>
          
        <?php endif; ?>
        </h1>
        </div>
    	<div id="bs-example-navbar-collapse-1" class="top-bar collapse navbar-collapse">
    				 <?php
				    wp_nav_menu( array(
				    'menu'       => 'secondary',
				    'theme_location'    => 'secondary',
				    'container' => '',
				    'depth'      => 1,
				    'menu_class' => 'nav navbar-nav top-nav',
                    'fallback_cb'       => 'wp_bootstrap_navwalker::fallback',
				    'walker'     => new wp_bootstrap_navwalker())
						);
        			?>
		   <!-- social -->

	<?php if(get_theme_mod('nfsocial_activate') != ""){ ?>
	 <ul class="nav navbar-nav navbar-right" style="margin-top: 10px;">
	<li><a href=" <?php echo get_theme_mod('fb_url');?>" class="fa fa-facebook-official fa-2x" ></a></li>
    <li><a href="<?php echo get_theme_mod('tw_url');?>"  class="fa fa-twitter-square fa-2x"></a></li>
    <li><a href="<?php echo get_theme_mod('gplus_url');?>" class="fa fa-google-plus-square fa-2x"></a></li>
    </ul>
    <?php }?>
    </div>
  </div>  
  </nav><!-- .site-branding -->
	</header><!-- #masthead -->

    <!-- #site-navigation2 -->
    <nav itemscope="itemscope" itemtype="http://schema.org/SiteNavigationElement" id="navbar2" class="navbar navbar-default" role="navigation" style="margin-bottom: 0px;">
        <div class="container-fluid mobile-masthead" style="background:<?php echo get_theme_mod('mainnavbg_color');?>;">
         <div class="navbar-header">
         <button class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-2">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
        </div>
          <div id="bs-example-navbar-collapse-2" class="collapse navbar-collapse">
         <?php
            wp_nav_menu( array(
                'menu'              => 'primary',
                'theme_location'    => 'primary',
                'depth'             => 2,
                'menu_class'        => 'nav navbar-nav main-nav',
                'fallback_cb'       => 'wp_bootstrap_navwalker::fallback',
                'walker'            => new wp_bootstrap_navwalker())
            );
        
            if ( get_theme_mod( 'search_setting' ) == 'yes' ) {?>

            <form class="form-inline navbar-form" role="search" id="searchform" method="get" action="<?php echo esc_url( home_url( '/' ) ); ?>">
            <input type="text" class="form-control" value="<?php echo get_search_query(); ?>" name="s" id="s" />
            <button type="submit" id="searchsubmit" value="<?php echo esc_attr_x( 'Search', 'submit button' ,'news-essence'); ?>" class="btn btn-secondary">Search</button> 
            </form>
            
        <?php } ?>
           
            </div>
            </div>
	</nav>
<!-- #site-navigation2 -->
	<div id="content" class="custom-background site-content container">
  <!-- #breaking-news row block-1-->
        <div class="row breaking-news " style="margin-top:0px!important;">
          <?php require get_template_directory(). '/inc/newsessence-newsticker.php'; ?>
        </div>
        <!-- end #breaking-news row block-1-->

        <!-- #top-ads row block-2-->
        <div id="block-2" class="row advert-banner">
        <?php dynamic_sidebar('top-adsbanner'); ?>
        </div>
        <!-- end of #top-ads row block-2-->