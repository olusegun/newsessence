<?php
/**
 * The template for displaying all single posts.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package news_essence
 */
get_header(); ?>

	<?php the_breadcrumb(); 
	if ( get_theme_mod('show_postsidebar_setting') == 'yes' ) { ?>
	<main id="main" class="col-sm-8 col-md-8 site-main" role="main" itemprop="mainContentOfPage" itemscope="itemscope" itemtype="http://schema.org/Blog">
	<?php } else { ?>
	<main id="main" class="col-sm-12 col-md-12 site-main" role="main" itemprop="mainContentOfPage" itemscope="itemscope" itemtype="http://schema.org/Blog">
	<?php } 

		while ( have_posts() ) : the_post();
			if ( get_theme_mod('show_postsidebar_setting') == 'yes' ) { 
				get_template_part( 'template-parts/content', 'sidebar', get_post_format() );
				
			}else{
					get_template_part( 'template-parts/content', get_post_format() );
						
			} 
			 the_post_navigation( array(
            'prev_text'                  => __( '<span aria-hidden="true">&larr;</span>Prev Post','news-essence' ),
            'next_text'                  => __( 'Next Post<span aria-hidden="true">&rarr;</span>','news-essence' ),
            'in_same_term'               => true,
            'taxonomy'                   => __( 'post_tag','news-essence' ),
            'screen_reader_text' => __( 'Continue Reading','news-essence' ),
        ) );
			// If comments are open or we have at least one comment, load up the comment template.
			if ( comments_open() || get_comments_number() ) :
				comments_template();
			endif;

		endwhile; // End of the loop.
		?>

		</main><!-- #main -->
		<?php if ( get_theme_mod('show_postsidebar_setting') == 'yes' ) { 
			  get_sidebar('right');
			} ?>
	</div><!-- #content -->
<?php
get_footer();