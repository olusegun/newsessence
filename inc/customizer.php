<?php
/**
 * news essence Theme Customizer.
 *
 * @package news_essence
 */

/**
 * Add postMessage support for site title and description for the Theme Customizer.
 *
 * @param WP_Customize_Manager $wp_customize Theme Customizer object.
 */
function news_essence_customize_register( $wp_customize ) {
	$wp_customize->get_setting( 'blogname' )->transport         = 'postMessage';
	$wp_customize->get_setting( 'blogdescription' )->transport  = 'postMessage';
	


	//theme color and link hover color
	$wp_customize->add_setting('theme_color',
        array(
            'default'     => '',
            'sanitize_callback' => 'sanitize_hex_color',
        )
    );
 
    $wp_customize->add_control( new WP_Customize_Color_Control($wp_customize,
            'theme_color',
            array(
                'label'      => __( 'Theme Color','news-essence'),
                'section'    => 'colors',
                'settings'   => 'theme_color'
            )
        )
    );

    //Main Nav BG color
	$wp_customize->add_setting('mainnavbg_color',
        array(
            'default'     => '',
            'sanitize_callback' => 'sanitize_hex_color',
        )
    );
 
    $wp_customize->add_control( new WP_Customize_Color_Control($wp_customize,
            'mainnavbg_color',
            array(
                'label'      => __( 'Main Nav BG Color','news-essence'),
                'section'    => 'colors',
                'settings'   => 'mainnavbg_color'
            )
        )
    );

    //Main Nav Link color
	$wp_customize->add_setting('navlink_color',
        array(
            'default'     => '',
            'sanitize_callback' => 'sanitize_hex_color',
        )
    );
 
    $wp_customize->add_control( new WP_Customize_Color_Control($wp_customize,
            'navlink_color',
            array(
                'label'      => __( 'Main Nav Link Color','news-essence'),
                'section'    => 'colors',
                'settings'   => 'navlink_color'
            )
        )
    );


	$wp_customize->add_setting('nf_logo',
			array('default' => '',
            	  'sanitize_callback' =>'esc_url_raw'
			 ));

		$wp_customize->add_control(new WP_Customize_Image_Control($wp_customize,'nf_logo', array(

			'label'		=>	_('Upload Logo to replace site name'),
			'section'	=>	'title_tagline',
			'settings'	=>	'nf_logo',
			)));


		/**
		* Add sections to customizer
		*/
			$wp_customize->add_panel('theme-options', array(
	      'title' => __( 'Theme settings' , 'news-essence' ),
	      'description'=> __( 'Custom theme settings.' , 'news-essence' ),
	      'priority' => 130
	   		));
		
			
			
			$wp_customize->add_section('essence_option_section', array(
			'title'		=>_('Homepage'),
			'Priority'	=> 20,
			'panel' => 'theme-options',

			));

			$wp_customize->add_section('post_page_section', array(
			'title'		=>_('Posts and Pages'),
			'Priority'	=> 30,
			'panel' => 'theme-options',

			));

			$wp_customize->add_section('nfsocial_section', array(
			'title'		=>_('Social Media'),
			'Priority'	=> 40,
			'panel' => 'theme-options',

			));

			$wp_customize->add_section('essence_other_section', array(
			'title'		=>_('Others'),
			'Priority'	=> 20,
			'panel' => 'theme-options',

			));
						
			

			//diable section header image
			$wp_customize->remove_section('header_image');
			$wp_customize->remove_section('static_front_page');
			$wp_customize->remove_control('header_textcolor');
			$wp_customize->remove_control('background_color');
			$wp_customize->remove_section('background_image');

			$wp_customize->add_setting('nfsocial_activate',
			array('default' => '',
            	  'sanitize_callback' =>'esc_url_raw'
			 ));


			$wp_customize->add_control('nfsocial_icon', array(

			'label'		=>	_('Social Icon'),
			'section'	=>	'nfsocial_section',
			'settings'	=>	'nfsocial_activate',
			'type'		=>	'checkbox',
			));


			$wp_customize->add_setting('fb_url', 
			array('default' => '',
            	  'sanitize_callback' =>'esc_url_raw'
			 ));

			$wp_customize->add_control('nfsocial_fb', array(

			'label'		=>	_('Facebook Url'),
			'section'	=>	'nfsocial_section',
			'settings'	=>	'fb_url',
			'type'		=>	'textbox',
			));


			$wp_customize->add_setting('tw_url',
			array('default' => '',
            	  'sanitize_callback' =>'esc_url_raw'
			 ));

			$wp_customize->add_control('nfsocial_tw', array(

			'label'		=>	_('Twitter Url'),
			'section'	=>	'nfsocial_section',
			'settings'	=>	'tw_url',
			'type'		=>	'textbox',
			));


			$wp_customize->add_setting('gplus_url',
			array('default' => '',
            	  'sanitize_callback' =>'esc_url_raw'
			 ));

			$wp_customize->add_control('nfsocial_gplus', array(

			'label'		=>	_('Google Plus Url'),
			'section'	=>	'nfsocial_section',
			'settings'	=>	'gplus_url',
			'type'		=>	'textbox',
			));
					


			// =====================
		    //  = Category Dropdown =
		    //  =====================
						    $args = array(
				    'hide_empty' => 0
				    
				);
		    $categories = get_categories($args);
			$cats = array();
			$i = 0;
			foreach($categories as $category){
				if($i==0){
					$default = $category->slug;
					$i++;
				}
				$cats[$category->slug] = $category->name;
			}

		 
			$wp_customize->add_setting('headline_cat', array(
				'default'        => '',
				'sanitize_callback' => 'sanitize_text_field'
			));
			$wp_customize->add_control( 'headline_cat_select_box', array(
				'settings' => 'headline_cat',
				'label'   => 'Choose Category for featured post',
				'section'  => 'essence_option_section',
				'type'    => 'select',
				'choices' => $cats,
			));

			$wp_customize->add_setting('show_exclusive_content', array(
				'default'        => 'no',
				'sanitize_callback' => 'sanitize_text_field'
			));
			$wp_customize->add_control( 'exclusive_select_box', array(
				'settings' => 'show_exclusive_content',
				'label'   => 'Show Exclusive Content?',
				'section'  => 'essence_option_section',
				'type'    => 'select',
				'choices'  => array(
				'yes'  => 'yes',
				'no' => 'no',
				),
			));


			$wp_customize->add_setting('exclusive_content_number', array(
				'default'        => '2',
				'sanitize_callback' => 'sanitize_text_field'
			));
			$wp_customize->add_control( 'exclusive_select_box_number', array(
				'settings' => 'exclusive_content_number',
				'label'   => 'Number of posts to show',
				'section'  => 'essence_option_section',
				'type'    => 'select',
				'choices'  => array(
				'2'  => '2',
				'4' => '4',
				),
			));

			$wp_customize->add_setting('exclusive_cat', array(
				'default'        => '',
				'sanitize_callback' => 'sanitize_text_field'
			));
			$wp_customize->add_control( 'exclusive_cat_select_box', array(
				'settings' => 'exclusive_cat',
				'label'   => 'exclusive Content category',
				'section'  => 'essence_option_section',
				'type'    => 'select',
				'choices' => $cats,
			));



			$wp_customize->add_setting('top_cat', array(
				'default'        => '',
				'sanitize_callback' => 'sanitize_text_field'
			));
			$wp_customize->add_control( 'top_cat_select_box', array(
				'settings' => 'top_cat',
				'label'   => 'Newsticker Category',
				'section'  => 'essence_other_section',
				'type'    => 'select',
				'choices' => $cats,
			));

			
			$wp_customize->add_setting('search_setting', array(
				'default'        => 'no',
				'sanitize_callback' => 'sanitize_text_field'
			));
			$wp_customize->add_control( 'search_select_box', array(
				'settings' => 'search_setting',
				'label'   => 'Enable search box',
				'section'  => 'essence_other_section',
				'type'    => 'select',
				'choices'  => array(
				'yes'  => 'yes',
				'no' => 'no',
				),
			));

			
			
			//post and page control
			$wp_customize->add_setting('show_postsidebar_setting', array(
				'default'        => 'no',
				'sanitize_callback' => 'sanitize_text_field'
			));
			$wp_customize->add_control( 'postsidebar_select_box', array(
				'settings' => 'show_postsidebar_setting',
				'label'   => 'Show sidebar on post?',
				'section'  => 'post_page_section',
				'type'    => 'select',
				'choices'  => array(
				'yes'  => 'yes',
				'no' => 'no',
				),
			));


			
			$wp_customize->add_setting('show_breadcrumbs_setting', array(
				'default'        => 'no',
				'sanitize_callback' => 'sanitize_text_field'
			));
			$wp_customize->add_control( 'breadcrumb_select_box', array(
				'settings' => 'show_breadcrumbs_setting',
				'label'   => 'Show breadcrumb in post and page',
				'section'  => 'post_page_section',
				'type'    => 'select',
				'choices'  => array(
				'yes'  => 'yes',
				'no' => 'no',
				),
			));
				
}
add_action( 'customize_register', 'news_essence_customize_register' );

/**
 * Binds JS handlers to make Theme Customizer preview reload changes asynchronously.
 */
function news_essence_customize_preview_js() {
	wp_enqueue_script( 'news_essence_customizer', get_template_directory_uri() . '/js/customizer.js', array( 'customize-preview' ), '20151215', true );
}
add_action( 'customize_preview_init', 'news_essence_customize_preview_js' );