<?php 
/**
* Essence-blog Register widget area
*/

function news_essence_widgets_init() {
if (function_exists('register_sidebar')) {

	register_sidebar( array(
		'name'          => esc_html__( 'Main Sidebar', 'news-essence' ),
		'id'            => 'sidebar',
		'description'   => esc_html__('Dsiplay widgets on posts and pages','news-essence'),
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h3 class="widget-title">',
		'after_title'   => '</h3>',
	) );
	register_sidebar( array(
		'name'          => esc_html__( 'Home Sidebar Top', 'news-essence' ),
		'id'            => 'sidebar-1',
		'description'   => esc_html__('Displays widget on home page','news-essence'),
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h3 class="widget-title">',
		'after_title'   => '</h3>',
	) );
	
	register_sidebar( array(
		'name'          => esc_html__( 'Home Sidebar Middle', 'news-essence' ),
		'id'            => 'sidebar-2',
		'description'   => esc_html__('Displays widget on home page','news-essence'),
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h3 class="widget-title">',
		'after_title'   => '</h3>',
	) );
		
	register_sidebar( array(
		'name'          => esc_html__( 'Home Sidebar Bottom', 'news-essence' ),
		'id'            => 'sidebar-3',
		'description'   => esc_html__('Displays widget on home page','news-essence'),
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h3 class="widget-title">',
		'after_title'   => '</h3>',
	) );
	
	
	register_sidebar(array(
		'name' 			=> esc_html__('Content Block 1','news-essence'),
		'id'   			=> 'widget-content-1',
		'description'   => esc_html__('Displays content along with sidebar middle.','news-essence'),
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h3 class="widget-title">',
		'after_title'   => '</h3>',
	));

	register_sidebar(array(
		'name' 			=> esc_html__('Content Block 2','news-essence'),
		'id'   			=> 'widget-content-2',
		'description'   => esc_html__('Displays content along with sidebar bottom.','news-essence'),
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h3 class="widget-title">',
		'after_title'   => '</h3>',
	));
	register_sidebar(array(
		'name' 			=> esc_html__('Content Block 3','news-essence'),
		'id'   			=> 'widget-content-3',
		'description'   => esc_html__('Displays full width content.','news-essence'),
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h3 class="widget-title">',
		'after_title'   => '</h3>',
	));
	

	register_sidebar(array(
		'name' 			=> esc_html__('Top banner','news-essence'),
		'id'   			=> 'top-adsbanner',
		'description'   => esc_html__('This display ads banner above the content on home page 728x90px.','news-essence'),
		'before_widget' => '<div id="%1$s" class="col-md-12 col-sm-12 col-xs-12 ads-container">',
		'after_widget'  => '</div>',
	));
	
	register_sidebar(array(
		'name' 			=> esc_html__('Middle banner','news-essence'),
		'id'   			=> 'middle-adsbanner',
		'description'   => esc_html__('Display ads banner in between content on home page 728x90px.','news-essence'),
		'before_widget' => '<div id="%1$s" class="col-md-12 col-sm-12 col-xs-12 ads-container">',
		'after_widget'  => '</div>',
	));

	register_sidebar(array(
		'name' 			=> esc_html__('Bottom banner','news-essence'),
		'id'   			=> 'bottom-adsbanner',
		'description'   => esc_html__('Display ads banner at the bottom on home page 728x90px.','news-essence'),
		'before_widget' => '<div id="%1$s" class="col-md-12 col-sm-12 col-xs-12 ads-container">',
		'after_widget'  => '</div>',
	));

	register_sidebar(array(
		'name' 			=> esc_html__('Category banner','news-essence'),
		'id'   			=> 'category-adsbanner',
		'description'   => esc_html__('Display ads banner(728x90px) on Category page).','news-essence'),
		'before_widget' => '<div id="%1$s" class="col-md-12 col-sm-12 col-xs-12 ads-container">',
		'after_widget'  => '</div>',
	));
	
  }
}
add_action( 'widgets_init', 'news_essence_widgets_init' );