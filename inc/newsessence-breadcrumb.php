<?php 
function the_breadcrumb() {
     if ( get_theme_mod( 'show_breadcrumbs_setting' ) == 'no' ) { //option to show breadcrumb
            return;
        }
    // Settings
    $home_title         = 'Home';
       
    // Get the query & post information
    global $post,$wp_query;
       
    // Do not display on the homepage
    if ( !is_front_page() ) {
       
        // Build the breadcrums
        echo '<ol class="breadcrumb" itemscope itemtype="http://schema.org/BreadcrumbList">';
           
        // Home page
        echo '<li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem"><span itemscope itemtype="http://schema.org/Thing" itemprop="item"><span itemprop="name"><a href="' . get_home_url() . '" title="' . $home_title . '">' . $home_title . '</a></span></span><meta itemprop="position" content="1" /></li>';
           
        if ( is_archive() && !is_author() && !is_tax() && !is_category() && !is_tag() ) {
              
            echo '<li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">' . post_type_archive_title($prefix, false) . '</li>';
              
        } else if ( is_archive() && is_tax() && !is_category() && !is_tag() ) {
              
            // If post is a custom post type
            $post_type = get_post_type();
              
            // If it is a custom post type display name and link
            if($post_type != 'post') {
                  
                $post_type_object = get_post_type_object($post_type);
                $post_type_archive = get_post_type_archive_link($post_type);
              
                echo '<li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem"><a itemprop="item" href="' . $post_type_archive . '" title="' . $post_type_object->labels->name . '"><span itemprop="name">' . $post_type_object->labels->name . '</span></a><meta itemprop="position" content="2" /></li>';
                
              
            }
              
            $custom_tax_name = get_queried_object()->name;
            echo '<li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">' . $custom_tax_name . '</li>';
              
        } else if ( is_single() ) {
              
            // If post is a custom post type
            $post_type = get_post_type();
              
            // If it is a custom post type display name and link
            if($post_type != 'post') {
                  
                $post_type_object = get_post_type_object($post_type);
                $post_type_archive = get_post_type_archive_link($post_type);
              
                echo '<li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem"><a itemprop="item" href="' . $post_type_archive . '" title="' . $post_type_object->labels->name . '"><span itemprop="name">' . $post_type_object->labels->name . '</span></a><meta itemprop="position" content="2" /></li>';
              
            }
              
            // Get post category info
            $category = get_the_category();
             
            if(!empty($category)) {
              
                // Get last category post is in
                
                $last_category = $category[count($category)-1];
                  
                // Get parent any categories and create array
                $get_cat_parents = rtrim(get_category_parents($last_category->term_id, true, ','),',');
                $cat_parents = explode(',',$get_cat_parents);
                  
                // Loop through parent categories and store in variable $cat_display
                $cat_display = '';
                foreach($cat_parents as $cat_parents) {
                    $cat_display .='<li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem"><span itemscope itemtype="http://schema.org/Thing" itemprop="item"><span itemprop="name">'.($cat_parents).'</span></span><meta itemprop="position" content="2" /></li>';
                   
                }
             
            }
              
           
              
            // Check if the post is in a category
            if(!empty($last_category)) {
                echo $cat_display;
                echo '<li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">' . get_the_title() . '</li>';
                  
            // Else if post is in a custom taxonomy
            } else if(!empty($cat_id)) {
                  
                echo '<li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem"><span itemscope itemtype="http://schema.org/Thing" itemprop="item"><span itemprop="name"><a href="' . $cat_link . '" title="' . $cat_name . '">' . $cat_name . '</a></span></span><meta itemprop="position" content="2" /></li>';
                echo '<li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">' . get_the_title() . '</li>';
              
            } else {
                  
                echo '<li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">' . get_the_title() . '</li>';
                  
            }
              
        } else if ( is_category() ) {
               
            // Category page
            echo '<li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">' . single_cat_title('', false) . '</li>';
               
        } else if ( is_page() ) {
               
            // Standard page
            if( $post->post_parent ){
                   
                // If child page, get parents 
                $anc = get_post_ancestors( $post->ID );
                   
                // Get parents in the right order
                $anc = array_reverse($anc);
                 $parents = '';  
                // Parent page loop
                foreach ( $anc as $ancestor ) {
                    $parents .= '<li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem"><span itemscope itemtype="http://schema.org/Thing" itemprop="item"><span itemprop="name"><a href="' . get_permalink($ancestor) . '" title="' . get_the_title($ancestor) . '">' . get_the_title($ancestor) . '</a></span></span><meta itemprop="position" content="2" /></li>';
                }
                   
                // Display parent pages
                echo $parents;
                   
                // Current page
                echo '<li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">' . get_the_title() . '</li>';
                   
            } else {
                   
                // Just display current page if not parents
                echo '<li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">' . get_the_title() . '</li>';
                   
            }
               
        } else if ( is_tag() ) {
               
            // Tag page
               
            // Get tag information
            $term_id        = get_query_var('tag_id');
            $taxonomy       =  __( 'post_tag','news-essence' );
            $args           = 'include=' . $term_id;
            $terms          = get_terms( $taxonomy, $args );
            $get_term_id    = $terms[0]->term_id;
            $get_term_slug  = $terms[0]->slug;
            $get_term_name  = $terms[0]->name;
               
            // Display the tag name
            echo '<li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">' . $get_term_name . '</li>';
           
        } elseif ( is_day() ) {
               
            // Day archive
               
            // Year link
            echo '<li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem"><span itemscope itemtype="http://schema.org/Thing" itemprop="item"><span itemprop="name"><a href="' . get_year_link( get_the_time('Y') ) . '" title="' . get_the_time('Y') . '">' . get_the_time('Y') . ' Archives</a></span></span><meta itemprop="position" content="2" /></li>';
               
            // Month link
            echo '<li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem"><span itemscope itemtype="http://schema.org/Thing" itemprop="item"><span itemprop="name"><a href="' . get_month_link( get_the_time('Y'), get_the_time('m') ) . '" title="' . get_the_time('M') . '">' . get_the_time('M') . ' Archives</a></span></span><meta itemprop="position" content="2" /></li>';
               
            // Day display
            echo '<li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">' . get_the_time('jS') . ' ' . get_the_time('M') . ' Archives</li>';
               
        } else if ( is_month() ) {
               
            // Month Archive
               
            // Year link
            echo '<li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem"><span itemscope itemtype="http://schema.org/Thing" itemprop="item"><span itemprop="name"><a href="' . get_year_link( get_the_time('Y') ) . '" title="' . get_the_time('Y') . '">' . get_the_time('Y') . ' Archives</a></span></span><meta itemprop="position" content="2" /></li>';
            
               
            // Month display
            echo '<li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">' . get_the_time('M') . ' Archives</li>';
               
        } else if ( is_year() ) {
               
            // Display year archive
            echo '<li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">' . get_the_time('Y') . ' Archives</li>';
               
        } else if ( is_author() ) {
               
            // Auhor archive
               
            // Get the author information
            global $author;
            $userdata = get_userdata( $author );
               
            // Display author name
            echo '<li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">' . 'Author: ' . $userdata->display_name . '</li>';
           
        } else if ( get_query_var('paged') ) {
               
            // Paginated archives
            echo '<li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">'.__('Page','news-essence') . ' ' . get_query_var('paged') . '</li>';
               
        } else if ( is_search() ) {
           
            // Search results page
            echo '<li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">Search results for: ' . get_search_query() . '</li>';
           
        } elseif ( is_404() ) {
               
            // 404 page
            echo '<li>' . 'Error 404' . '</li>';
        }
       
        echo '</ol>';
           
    }
       
}