<?php
/**
*Add Headline post
*/
		$cat = get_theme_mod('headline_cat');
		if ( $cat == '' ) { //option to show breadcrumb
            return;
        }
			$the_query = new WP_Query( array(
			
			'orderby' 			=> 'date',
			'order'				=> 'DESC',
			'category_name'		=> $cat,
			'posts_per_page'	=> 3,
			'post_status' => 'publish',
			'ignore_sticky_posts' => false,
						
		));


		  $i=1; while ( $the_query->have_posts() ) : $the_query->the_post();
		  
		  if ($i == 1){ ?>
		  	<div class="col-sm-7 col-md-7 c1">
		 	<article itemscope="itemscope" itemtype="http://schema.org/BlogPosting" itemprop="blogPost" class="col-md-12 col-sm-12 featured-thumb-lg box-shadow--6dp" style="padding-left: 0px; padding-right: 0px;"  id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
		 		
		    			<div class="panel panel-default">
			    		  		<?php echo '<a itemprop="image" class="featured-img-lg img-responsive" data-block="3" data-position="4" style="background-image:url('.wp_get_attachment_url( get_post_thumbnail_id() ).');" href='.get_permalink().'></a>'; ?>
	  					  		<div class="caption-thumb-lg">
			  					  		<?php the_title( '<h2 itemprop="headline" class="news_title entry-title"><a data-block="4" data-position="4" href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h2>' );?>
			  					
			  					<small class="pull-right" style="margin: 5px;"><i class="fa fa-calendar-o" aria-hidden="true"></i><?php echo human_time_diff( get_the_time( 'U' ), current_time('timestamp') ); ?>&nbsp;ago
			  					</small>
		 					  </div>
								</div>
								
			</article>
			</div>
			<div class="col-sm-5 col-md-5 c2">
		<?php
		}
		else{ ?>
			 <div class="row" style="margin: 0!important;">
		<article itemscope="itemscope" itemtype="http://schema.org/BlogPosting" itemprop="blogPost" class="col-md-12 col-sm-12 featured-thumb-sm box-shadow--6dp" style="margin-bottom: 4px;" id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
		 		
		    			<div class="panel panel-default">
			    		  		<?php echo '<a itemprop="image" class="featured-img-sm img-responsive" data-block="3" data-position="4" style="background-image:url('.wp_get_attachment_url( get_post_thumbnail_id() ).');" href='.get_permalink().'></a>'; ?>
	  					  		<div class="caption-thumb-sm">
			  					  		<?php the_title( '<h2 itemprop="headline" class="news_title-sm entry-title"><a data-block="4" data-position="4" href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h2>' );?>
			  					
	 					  		<small class="pull-right" style="margin: 5px;"><i class="fa fa-calendar-o" aria-hidden="true"></i><?php echo human_time_diff( get_the_time( 'U' ), current_time('timestamp') ); ?>&nbsp;ago</small>
		 					  </div>
								</div>
							
							</article>
			</div>

		 <?php } ?>
		
		 <?php wp_reset_postdata();
		$i++; endwhile;
		?>		
		 </div>