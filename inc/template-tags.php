<?php
/**
 * Custom template tags for this theme.
 *
 * Eventually, some of the functionality here could be replaced by core features.
 *
 * @package news_essence
 */

if ( ! function_exists( 'news_essence_posted_on' ) ) :
/**
 * Prints HTML with meta information for the current post-date/time and author.
 */
function news_essence_posted_on() {
	$time_string = '<time itemprop="datePublished" class="entry-date published updated" datetime="%1$s">%2$s</time>';
	if ( get_the_time( 'U' ) !== get_the_modified_time( 'U' ) ) {
		$time_string = '<time itemprop="datePublished" class="entry-date published" datetime="%1$s">%2$s</time><time class="updated" datetime="%3$s">%4$s</time>';
	}

	$time_string = sprintf( $time_string,
		esc_attr( get_the_date( 'c' ) ),
		esc_html( get_the_date() ),
		esc_attr( get_the_modified_date( 'c' ) ),
		esc_html( get_the_modified_date() )
	);
	$time_string = human_time_diff( get_the_time('U'), current_time('timestamp') );
	$posted_on = sprintf(
		esc_html_x( '%s', 'post date', 'news-essence' ),
		'<i class="fa fa-calendar" aria-hidden="true"></i>' . $time_string . ' Ago'
	);

	$byline = sprintf(
		esc_html_x('%s', 'post author', 'news-essence' ),
		'<i itemscope="itemscope" itemtype="http://schema.org/Person" itemprop="author" class="fa fa-user" aria-hidden="true"><a style="color: rgb(68, 68, 68);" href="' . esc_url( get_author_posts_url( get_the_author_meta( 'ID' ) ) ) . '">' .' '. esc_html( get_the_author() ) . '</a></i>'
	);

	echo '<span class="posted-on">' . $posted_on . '</span><span class="byline"> ' . $byline . '</span>'; // WPCS: XSS OK.

}
endif;

if ( ! function_exists( 'news_essence_entry_footer' ) ) :
/**
 * Prints HTML with meta information for the categories, tags and comments.
 */
function news_essence_entry_footer() {

	//edit post
	edit_post_link(
		sprintf(
			/* translators: %s: Name of current post */
			esc_html__( 'Edit Post%s', 'news-essence' ),
			the_title( '<span class="screen-reader-text">"', '"</span>', false )
		),
		'<span class="fa fa-pencil-square-o">&nbsp;',
		'</span>'
	);
	// Hide category and tag text for pages.

	if ( 'post' === get_post_type() ) {

		/* translators: used between list items, there is a space after the comma */
		$categories_list = get_the_category_list(esc_html__( ' ', 'news-essence'));
		if ( $categories_list && news_essence_categorized_blog() ) {
			printf( '<nav aria-label="..."><ul class="pager"><li>' . esc_html__( 'Posted in %1$s', 'news-essence' ) . '</li></ul></nav>', $categories_list ); // WPCS: XSS OK.
		}
		
		/* translators: used between list items, there is a space after the comma */
		$tags_list = get_the_tag_list( '<li>', esc_html__( ' ', 'news-essence' ),'</li>' );
		if ( $tags_list ) {
			printf( '<nav aria-label="..."><ul class="pager" itemprop="keywords">' . esc_html__( 'TAGS: %1$s', 'news-essence' ) . '</ul></nav>', $tags_list ); // WPCS: XSS OK.
		}
	}

	if ( ! is_single() && ! post_password_required() && ( comments_open() || get_comments_number() ) ) {
		echo '<span class="comments-link">';
		/* translators: %s: post title */
		comments_popup_link( sprintf( wp_kses( __( 'Leave a Comment<span class="screen-reader-text"> on %s</span>', 'news-essence' ), array( 'span' => array( 'class' => array() ) ) ), get_the_title() ) );
		echo '</span>';
	}
}
endif;
/**
 * Returns true if a blog has more than 1 category.
 *
 * @return bool
 */
function news_essence_categorized_blog() {
	if ( false === ( $all_the_cool_cats = get_transient( 'news_essence_categories' ) ) ) {
		// Create an array of all the categories that are attached to posts.
		$all_the_cool_cats = get_categories( array(
			'fields'     => 'ids',
			'hide_empty' => 1,
			// We only need to know if there is more than one category.
			'number'     => 2,
		) );

		// Count the number of categories that are attached to the posts.
		$all_the_cool_cats = count( $all_the_cool_cats );

		set_transient( 'news_essence_categories', $all_the_cool_cats );
	}

	if ( $all_the_cool_cats > 1 ) {
		// This blog has more than 1 category so news_essence_categorized_blog should return true.
		return true;
	} else {
		// This blog has only 1 category so news_essence_categorized_blog should return false.
		return false;
	}
}


function essence_link_pages( $args = array () ) {
    $defaults = array(
        'before'      => '<p>' . __('Pages:','news-essence'),
        'after'       => '</p>',
        'before_link' => '',
        'after_link'  => '',
        'current_before' => '',
        'current_after' => '',
        'link_before' => '',
        'link_after'  => '',
        'pagelink'    => '%',
        'echo'        => 1

    );

    $r = wp_parse_args( $args, $defaults );
    $r = apply_filters( 'wpessence_link_pages_args', $r );
    extract( $r, EXTR_SKIP );

    global $page, $numpages, $multipage, $more, $pagenow;

    if ( ! $multipage )
    {
        return;
    }

    $output = $before;

    for ( $i = 1; $i < ( $numpages + 1 ); $i++ )
    {
        $j       = str_replace( '%', $i, $pagelink );
        $output .= ' ';

        if ( $i != $page || ( ! $more && 1 == $page ) )
        {
            $output .= "{$before_link}" . _wp_link_page( $i ) . "{$link_before}{$j}{$link_after}</a>{$after_link}";
        }
        else
        {
            $output .= "{$current_before}{$link_before}<a>{$j}</a>{$link_after}{$current_after}";
        }
    }

    print $output . $after;
}

/**
 * Flush out the transients used in news_essence_categorized_blog.
 */
function news_essence_category_transient_flusher() {
	if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
		return;
	}
	// Like, beat it. Dig?
	delete_transient( 'news_essence_categories' );
}
add_action( 'edit_category', 'news_essence_category_transient_flusher' );
add_action( 'save_post',     'news_essence_category_transient_flusher' );