<?php
 /**
 *
 * @package news_essence
 */

/**
 * Set up Theme css option
 *
 * 
 */


function essence_customize_css()
{
    ?>
         <style type="text/css" id="custom-theme-css">
             .cat-title, .widget-title { border-bottom: 2px solid <?php echo get_theme_mod('theme_color'); ?>; } 
             .site-footer  { border-top: 1px solid <?php echo get_theme_mod('theme_color'); ?>;}
             a:focus, a:hover { color:<?php echo get_theme_mod('theme_color'); ?>;}
             .navbar-default .navbar-nav > li > a,
             .dropdown-menu > li > a { color:<?php echo get_theme_mod('navlink_color'); ?> !important;}
             .dropdown-menu { background-color:<?php echo get_theme_mod('mainnavbg_color'); ?>;}
             .pagination > .active > a, .pagination > .active > a:focus, .pagination > .active > a:hover, .pagination > .active > span, .pagination > .active > span:focus, .pagination > .active > span:hover {
		    background-color: <?php echo get_theme_mod('mainnavbg_color'); ?>;
    		border-color: <?php echo get_theme_mod('mainnavbg_color'); ?>;
			}
			.wp-pagenavi > .current {
		    background-color: <?php echo get_theme_mod('mainnavbg_color'); ?>;
    		border-color: <?php echo get_theme_mod('mainnavbg_color');?> !important;
			}
			.pagination > li > a:focus, .pagination > li > a:hover, .pagination > li > span:focus, .pagination > li > span:hover { color: <?php echo get_theme_mod('mainnavbg_color'); ?>;}
			.wp-pagenavi > a:focus, .wp-pagenavi  > a:hover { color: <?php echo get_theme_mod('mainnavbg_color'); ?>;}

			.nav-previous >a:focus, .nav-previous > a:hover, .nav-next > a:focus, .nav-next > a:hover  {   color: <?php echo get_theme_mod('mainnavbg_color'); ?>;}
			
             @media (max-width: 767px){
             .navbar-default .navbar-nav .open .dropdown-menu > li > a {
   			 color: <?php echo get_theme_mod('navlink_color'); ?> !important;}
   			}
}

         </style>
    <?php
}