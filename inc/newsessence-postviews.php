<?php
/**
	 * Post view
	
	 */
	function wpb_set_post_views($postID) {
		    $count_key = 'wpb_post_views_count';
		    $count = get_post_meta($postID, $count_key, true);
		    if($count==''){
		        $count = 0;
		        delete_post_meta($postID, $count_key);
		        add_post_meta($postID, $count_key, '0');
		    }else{
		        $count++;
		        update_post_meta($postID, $count_key, $count);
		    }
		 }
		//To keep the count accurate, lets get rid of prefetching
		remove_action( 'wp_head', 'adjacent_posts_rel_link_wp_head', 10, 0);


	function wpb_track_post_views ($post_id) {
		    if ( !is_single() ) return;
		    if ( empty ( $post_id) ) {
		        global $post;
		        $post_id = $post->ID;    
		    }
		    wpb_set_post_views($post_id);
		}

		add_action( 'wp_head', 'wpb_track_post_views');

	function wpb_get_post_views($postID){
		    $count_key = 'wpb_post_views_count';
		    $count = get_post_meta($postID, $count_key, true);
		    if($count==''){
		        delete_post_meta($postID, $count_key);
		        add_post_meta($postID, $count_key, '0');
		        return "0 View";
		    }
		    return $count;
		}

// Add it to a column in WP-Admin
		add_filter('manage_posts_columns', 'posts_column_views');
		add_action('manage_posts_custom_column', 'posts_custom_column_views',5,2);
		function posts_column_views($defaults){
		    $defaults['post_views'] = __('Views','news-essence');
		    return $defaults;
		}
	
	function posts_custom_column_views($column_name, $id){
		    if($column_name === 'post_views'){
		        echo wpb_get_post_views(get_the_ID());
		    }
		}

	 function no_front_sticky_wpse_98680($qry) {
	  if (is_front_page()) {
	    $qry->set('post__not_in',get_option( 'sticky_posts' ));
	  }
	}
add_action('pre_get_posts','no_front_sticky_wpse_98680');