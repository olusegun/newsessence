<?php
/**
* Exclusive Content
*/

	$cat_id = get_theme_mod( 'exclusive_cat' );
	$num = get_theme_mod( 'exclusive_content_number' );
		$args= array(
			
			'orderby' 			=> 'date',
			'order'				=> 'DESC',
			'category_name'		=>	$cat_id,
			'posts_per_page'	=> $num,
			'post_type'			=>'post',
			'no_found_rows' => true,
			'post_status' => 'publish',
			'ignore_sticky_posts' => true,
					
		);
		
		$q = new WP_Query($args); ?>
				<div class="row">
				<?php while ( $q->have_posts() ) : $q->the_post(); ?>
				<div class="col-xm-12 col-sm-6 col-md-6" style="margin-top: 5px;">
				<div class="exclusive-r box-shadow--2dp">
				<div class="exclusive-img">
				<?php if ( has_post_thumbnail() ) { 
					echo '<a class="img-r img-responsive" data-block="3" data-position="4" style="background-image:url('.wp_get_attachment_url( get_post_thumbnail_id() ).');" href='.get_permalink().'></a>'; 
				}else {
				echo '<a class="img-r img-responsive" data-block="3" data-position="4" style="background-image:url('.get_stylesheet_directory_uri().'/img/no-image.jpg);" href='.get_permalink().'></a>';								
					}?>
				</div>
				<div class="exclusive-body">
					<p class="body-caption">
					<?php the_title( '<h2 class="news_title entry-title"><a data-block="4" data-position="4" href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h2>' );?>
					</p>
					<span class="exclusive-time"><?php echo human_time_diff( get_the_time( 'U' ), current_time('timestamp') ); ?> Ago</span>
				</div>
				</div>
				</div>
				<?php endwhile;	?>
				</div>
				<?php wp_reset_postdata(); 