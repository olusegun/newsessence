<?php
/**
*Add Newsticker post
*/
		$cat = get_theme_mod('top_cat');
		
		$the_query = new WP_Query( array(
			
			'orderby' 			=> 'date',
			'order'				=> 'DESC',
			'category_name'		=> $cat,
			'posts_per_page'	=> 3,
			'post_status' => 'publish',
			'ignore_sticky_posts' => false,
						
		));

		 if ($cat == ''){
		 	return;
		 }
		 if ( $the_query->have_posts() ) { ?>
		 		<div class="col-lg-12 col-md-12 col-sm-12 ticker">
		 		<!--<strong>JuSt In:&nbsp;</strong>-->
		 		<ul>
		 		<?php while ( $the_query->have_posts() ) {
							$the_query->the_post(); ?>
					<li><a style="color:#fff!important;" data-block="0" data-position="0" href="<?php the_permalink(); ?>"><?php the_title(); ?></a></li>
      		 	
				<?php } ?>
				</ul>
				</div>
				
		<?php
		}
		wp_reset_postdata();