<?php
/**
* Newsessence Full Width Category Content widget
*/


function register_fullwidth_category_content_widget(){

	register_widget('newsessence_fullwidth_category_content_widget');
}

add_action('widgets_init', 'register_fullwidth_category_content_widget');

class newsessence_fullwidth_category_content_widget extends WP_Widget {
	
	public function __construct()
	{
		
		parent::__construct(
				'newsessence_fullwidth_category_widget',
				_('NewsEssence - Full Width Content Category'),
				 array('description' =>_('Display posts for a specific category in full width'),)
		);
	}

	public function form($instance){
		
		$title = isset( $instance['title'] ) ? esc_attr( $instance['title'] ) : '';
		$number = isset( $instance['number'] ) ? absint( $instance['number'] ) : 8;
		$cat_id = isset( $instance['cat_id'] ) ? esc_attr( $instance['cat_id'] ) : 0;
		?>

		<p><label for="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>"><?php _e( 'Title:', 'news-essence' ); ?></label>
		<input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'title' ) ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>" /></p>
		<p><label for="<?php echo esc_attr( $this->get_field_id( 'number' ) ); ?>"><?php _e( 'Number of posts to show:', 'news-essence' ); ?></label>
		<input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'number' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'number' ) ); ?>" type="text" value="<?php echo esc_attr( $number ); ?>" size="3" /></p>
		<p><label for="<?php echo esc_attr( $this->get_field_id( 'cat_id' ) ); ?>"><?php _e( 'Category:', 'news-essence' ); ?></label>
		<?php wp_dropdown_categories( 'name='.$this->get_field_name( 'cat_id' ).'&class=widefat&show_option_all=All&hide_empty=0&hierarchical=1&depth=2&selected='.$cat_id); ?></p>
		<?php
	}

	public function update($new_instance, $old_instance){
		$instance['title'] = strip_tags( $new_instance['title'] );
		$instance['number'] = (int) $new_instance['number'];
		$instance['cat_id'] = (int) $new_instance['cat_id'];
		return $instance;

	}

	public function widget($args, $instance) {

		$title = ( ! empty( $instance['title'] ) ) ? $instance['title'] : '';
		$title = apply_filters( 'widget_title', $title, $instance, $this->id_base );
		$number = ( ! empty( $instance['number'] ) ) ? absint( $instance['number'] ) : 8;
		$cat_id = ( ! empty( $instance['cat_id'] ) ) ? absint( $instance['cat_id'] ) : 0;

		$args = array(
			
			'orderby' 			=> 'date',
			'order'				=> 'DESC',
			'cat'				=>	$cat_id,
			'posts_per_page'	=> $number,
			'post_type'			=>'post',
			'no_found_rows' => true,
			'post_status' => 'publish',
			'ignore_sticky_posts' => true,
					
		);

		$fullcatpost = new WP_Query($args);

		$s1 = array(1,2,3,12,13,14,17,18,23,24,25,34,35,36,45,46,47,56,57,58,67,68,69,78,79,80,89,90,91);
				$s2 = array(4,5,6,7,15,16,17,18,26,27,28,29,37,38,39,40,48,49,50,51,59,60,61,62,70,71,72,73,81,82,83,84,92,93,94,95);
				$s21 = array(4,15,26,37,48,59,70,81,92);
				$s3 = array(8,9,10,11,19,20,21,22,30,31,32,33,41,42,43,44,52,53,54,55,63,64,65,66,74,75,76,77,85,86,87,88,96,97,98,99);
				$s31 = array(9,20,31,42,53,64,75,86,97);
				

				/* Start the Loop */
				?>
		  	<h3 class="cat-title"><?php echo $title; ?> </h3>
		  	<?php $i = 1; while ( $fullcatpost->have_posts() ) : $fullcatpost->the_post(); 
					if (in_array($i, $s1)){ ?>
		            <article itemscope="itemscope" itemtype="http://schema.org/BlogPosting" itemprop="blogPost" class="col-md-4 col-sm-4"  id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
			    		<div class="panel panel-default box-shadow--2dp news-cell--big">
			    		  		<?php echo '<a itemprop="image" class="news-img img-responsive" data-block="3" data-position="4" style="background-image:url('.wp_get_attachment_url( get_post_thumbnail_id() ).');" href='.get_permalink().'></a>'; ?>
	  					  	<div class="panel-body news_body">
			  					  		<?php the_title( '<h2 itemprop="headline" class="news_title entry-title"><a data-block="4" data-position="4" href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h2>' );?>
			  				</div>
	 					    <div class="panel-footer footer-social">
		 					  	<ul class="nav info-social">
		 					  	<li class="item-info news-time"><i class="fa fa-calendar-o" aria-hidden="true"> <?php echo human_time_diff( get_the_time( 'U' ), current_time('timestamp') ); ?> ago</i></li>
		 					  		<li class="item-info info-views"><i class="fa fa-eye" aria-hidden="true"> <?php echo wpb_get_post_views(get_the_ID()); ?> </i></li>
		 					        <li class="item-info info-comment"><i class="fa fa-comment" aria-hidden="true"></i><?php comments_number( '0', '', '' ); ?></li>
		 					        </ul>
		 					  </div>
						</div>
		            </article>

		    	<?php } 

		    	if (in_array($i, $s2))
		    	{ 

				   	if($i % 11 == 4) {?>
						<article itemscope="itemscope" itemtype="http://schema.org/BlogPosting" itemprop="blogPost" class="col-md-4 col-sm-4"  id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
				    		 	<div class="panel panel-default box-shadow--2dp news-cell--small">
					    		 <div class="panel-body news_body">
			  					  		<?php the_title( '<h2 itemprop="headline" class="news_title entry-title"><a data-block="4" data-position="4" href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h2>' );?>
			  					</div>
								<div class="panel-footer footer-social">
		 					  	<ul class="nav info-social">
		 					  		<li class="item-info news-time"><i class="fa fa-calendar-o" aria-hidden="true"> <?php echo human_time_diff( get_the_time( 'U' ), current_time('timestamp') ); ?> ago</i></li>
		 					  		<li class="item-info info-views"><i class="fa fa-eye" aria-hidden="true"> <?php echo wpb_get_post_views(get_the_ID()); ?> </i></li>
		 					        <li class="item-info info-comment"><i class="fa fa-comment" aria-hidden="true"></i><?php comments_number( '0', '', '' ); ?></li>
		 					   </ul>
		 					  </div>
							 		</div>
							 	<?php if (in_array($number, $s21)):?> </article> <?php endif; ?>
						<?php } 

					if ($i % 11 == 5) { ?>

								<div class="panel panel-default box-shadow--2dp news-cell--small">
					    		 <div class="panel-body news_body">
			  					  		<?php the_title( '<h2 itemprop="headline" class="news_title entry-title"><a data-block="4" data-position="4" href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h2>' );?>
			  				</div>
			 					 <div class="panel-footer footer-social">
		 					  	<ul class="nav info-social">
		 					  	<li class="item-info news-time"><i class="fa fa-calendar-o" aria-hidden="true"> <?php echo human_time_diff( get_the_time( 'U' ), current_time('timestamp') ); ?> ago</i></li>
		 					  		<li class="item-info info-views"><i class="fa fa-eye" aria-hidden="true"> <?php echo wpb_get_post_views(get_the_ID()); ?> </i></li>
		 					        <li class="item-info info-comment"><i class="fa fa-comment" aria-hidden="true"></i><?php comments_number( '0', '', '' ); ?></li>
		 					        </ul>
		 					  </div>
								</div>
								</article>

					<?php }

						if ( ($i % 11 == 6)  || ($i % 11== 7)){ ?>

							<article itemscope="itemscope" itemtype="http://schema.org/BlogPosting" itemprop="blogPost" class="col-md-4 col-sm-4"  id="post-<?php the_ID(); ?>" <?php post_class(); ?>> 
								<div class="panel panel-default box-shadow--2dp news-cell--big">
			    		  		<?php echo '<a itemprop="image" class="news-img img-responsive" data-block="3" data-position="4" style="background-image:url('.wp_get_attachment_url( get_post_thumbnail_id() ).');" href='.get_permalink().'></a>'; ?>
	  					  		<div class="panel-body news_body">
			  					  		<?php the_title( '<h2 itemprop="headline" class="news_title entry-title"><a data-block="4" data-position="4" href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h2>' );?>
			  				</div>
	 					       <div class="panel-footer footer-social">
		 					  	<ul class="nav info-social">
		 					  	<li class="item-info news-time"><i class="fa fa-calendar-o" aria-hidden="true"> <?php echo human_time_diff( get_the_time( 'U' ), current_time('timestamp') ); ?> ago</i></li>
		 					  		<li class="item-info info-views"><i class="fa fa-eye" aria-hidden="true"> <?php echo wpb_get_post_views(get_the_ID()); ?> </i></li>
		 					        <li class="item-info info-comment"><i class="fa fa-comment" aria-hidden="true"></i><?php comments_number( '0', '', '' ); ?></li>
		 					        </ul>
		 					  </div>		 					       
						        </div>
							    </article>

						<?php  }
							
				} 
					
				
					if (in_array($i, $s3)){

							if ( $i % 11 == 8 || $i % 11 == 0){ ?>

							<article itemscope="itemscope" itemtype="http://schema.org/BlogPosting" itemprop="blogPost" class="col-md-4 col-sm-4"  id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
		    					<div class="panel panel-default box-shadow--2dp news-cell--big">
			    		  		<?php echo '<a itemprop="image" class="news-img img-responsive" data-block="3" data-position="4" style="background-image:url('.wp_get_attachment_url( get_post_thumbnail_id() ).');" href='.get_permalink().'></a>'; ?>
	  					  		<div class="panel-body news_body">
			  					  		<?php the_title( '<h2 itemprop="headline" class="news_title entry-title"><a data-block="4" data-position="4" href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h2>' );?>
			  				</div>
	 					  		<div class="panel-footer footer-social">
		 					  	<ul class="nav info-social">
		 					  	<li class="item-info news-time"><i class="fa fa-calendar-o" aria-hidden="true"> <?php echo human_time_diff( get_the_time( 'U' ), current_time('timestamp') ); ?> ago</i></li>
		 					  		<li class="item-info info-views"><i class="fa fa-eye" aria-hidden="true"> <?php echo wpb_get_post_views(get_the_ID()); ?> </i></li>
		 					        <li class="item-info info-comment"><i class="fa fa-comment" aria-hidden="true"></i><?php comments_number( '0', '', '' ); ?></li>
		 					        </ul>
		 					  </div>
								</div>
							</article>

						<?php	} 

								if($i % 11 == 9 ) { ?>

								<article itemscope="itemscope" itemtype="http://schema.org/BlogPosting" itemprop="blogPost" class="col-md-4 col-sm-4"  id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
				    		 		<div class="panel panel-default box-shadow--2dp news-cell--small">
					    		 	<div class="panel-body news_body">
			  					  		<?php the_title( '<h2 itemprop="headline" class="news_title entry-title"><a data-block="4" data-position="4" href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h2>' );?>
			  				</div>
	  					  			
				 				<div class="panel-footer footer-social">
		 					  	<ul class="nav info-social">
		 					  	<li class="item-info news-time"><i class="fa fa-calendar-o" aria-hidden="true"> <?php echo human_time_diff( get_the_time( 'U' ), current_time('timestamp') ); ?> ago</i></li>
		 					  		<li class="item-info info-views"><i class="fa fa-eye" aria-hidden="true"> <?php echo wpb_get_post_views(get_the_ID()); ?> </i></li>
		 					        <li class="item-info info-comment"><i class="fa fa-comment" aria-hidden="true"></i><?php comments_number( '0', '', '' ); ?></li>
		 					        </ul>
		 					  </div>
							 		</div>
							 	<?php if (in_array($number, $s31)):?> </article> <?php endif; ?>

								<?php } 

								if($i % 11 == 10){ ?>

									<div class="panel panel-default box-shadow--2dp news-cell--small">
							    		<div class="panel-body news_body">
			  					  		<?php the_title( '<h2 itemprop="headline" class="news_title entry-title"><a data-block="4" data-position="4" href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h2>' );?>
			  				</div>
			 					<div class="panel-footer footer-social">
		 					  	<ul class="nav info-social">
		 					  	<li class="item-info news-time"><i class="fa fa-calendar-o" aria-hidden="true"> <?php echo human_time_diff( get_the_time( 'U' ), current_time('timestamp') ); ?> ago</i></li>
		 					  		<li class="item-info info-views"><i class="fa fa-eye" aria-hidden="true"> <?php echo wpb_get_post_views(get_the_ID()); ?> </i></li>
		 					        <li class="item-info info-comment"><i class="fa fa-comment" aria-hidden="true"></i><?php comments_number( '0', '', '' ); ?></li>
		 					        </ul>
		 					  </div>
								</div>
								</article>

						<?php		}
						
					}
		$i++; endwhile;
			wp_reset_postdata();

	 } 
}