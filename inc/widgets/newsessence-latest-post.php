<?php
/**
* NewsEssence recent post
*/


function register_newsessence_latest_post_widget(){

	register_widget('register_newsessence_latest_post_widget');
}

add_action('widgets_init', 'register_newsessence_latest_post_widget');

class register_newsessence_latest_post_widget extends WP_Widget {
	
	public function __construct()
	{
		
		parent::__construct(
				'essence_latest_post_widget',
				_('NewsEssence - Latest Post'),
				 array('description' =>_('Display latest posts'),)
		);
	}

	public function widget($args, $instance) {

		$title = ( ! empty( $instance['title'] ) ) ? $instance['title'] : '';
		$title = apply_filters( 'widget_title', $title, $instance, $this->id_base );
		
		$number = ( ! empty( $instance['number'] ) ) ? absint( $instance['number'] ) : 0;
		$show_date = isset( $instance['show_date'] ) ? $instance['show_date'] : false;

		$the_latest = new WP_Query( apply_filters( 'widget_posts_args', array(
			'posts_per_page'      => $number,
			'no_found_rows'       => true,
			'post_status'         => 'publish',
			'ignore_sticky_posts' => true
		) ) );
		 

		
		 if ($number == ''){
		 	return;
		 } 

		 	if ( $title) : ?>
		 	<h3 class="cat-title"> <?php echo $title; ?> </h3>
		    <?php endif; ?>
		<div class="list-group">
		<?php while ( $the_latest->have_posts() ) : $the_latest->the_post(); ?>
	
		<a class="list-group-item url" style="padding-bottom: 20px; line-height: 1.3em;" href="<?php the_permalink(); ?>"><?php get_the_title() ? the_title() : the_ID(); ?>
		<?php if ( $show_date ) : ?>
          <small class="pull-right"><em><?php echo get_the_date(); ?></em></small>
        <?php endif; ?>
        </a>

       <?php endwhile; ?>
		</div>
		<?php
		wp_reset_postdata();
	 }

	public function update( $new_instance, $old_instance ) {
		$instance = $old_instance;
		$instance['title'] = sanitize_text_field( $new_instance['title'] );
		$instance['number'] = (int) $new_instance['number'];
		$instance['show_date'] = isset( $new_instance['show_date'] ) ? (bool) $new_instance['show_date'] : false;
		return $instance;
	}

	public function form($instance){
		
		$title = isset( $instance['title'] ) ? esc_attr( $instance['title'] ) : '';
		$number = isset( $instance['number'] ) ? absint( $instance['number'] ) : '';
        $show_date = isset( $instance['show_date'] ) ? (bool) $instance['show_date'] : false;
        ?>

		<p><label for="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>"><?php _e( 'Title:', 'news-essence' ); ?></label>
		<input class="widefat" placeholder="Leave empty to hide title" id="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'title' ) ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>" /></p>
		<p><label for="<?php echo esc_attr( $this->get_field_id( 'number' ) ); ?>"><?php _e( 'Number of posts to show:', 'news-essence' ); ?></label>
		<input class="widefat" placeholder="Enter number of post" id="<?php echo esc_attr( $this->get_field_id( 'number' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'number' ) ); ?>" type="text" value="<?php echo esc_attr( $number ); ?>" size="3" /></p>
		<p><input class="checkbox" type="checkbox" <?php checked( $show_date ); ?> id="<?php echo $this->get_field_id( 'show_date' ); ?>" name="<?php echo $this->get_field_name( 'show_date' ); ?>" />
        <label for="<?php echo $this->get_field_id( 'show_date' ); ?>"><?php _e( 'Display post date?','news-essence' ); ?></label></p>

		<?php
	}

} 