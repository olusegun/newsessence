<?php
/**
* Trending Post based on views
*/


function register_category_content_widget(){

	register_widget('newsessence_category_content_widget');
}

add_action('widgets_init', 'register_category_content_widget');

class newsessence_category_content_widget extends WP_Widget {
	
	public function __construct()
	{
		
		parent::__construct(
				'newsessence_category_widget',
				_('NewsEssence - Category Post'),
				 array('description' =>_('Display posts for a specific category'),)
		);
	}

	public function form($instance){
		
		$title = isset( $instance['title'] ) ? esc_attr( $instance['title'] ) : '';
		$number = isset( $instance['number'] ) ? absint( $instance['number'] ) : 5;
		$cat_id = isset( $instance['cat_id'] ) ? esc_attr( $instance['cat_id'] ) : 0;
		?>

		<p><label for="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>"><?php _e( 'Title:', 'news-essence' ); ?></label>
		<input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'title' ) ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>" /></p>
		<p><label for="<?php echo esc_attr( $this->get_field_id( 'number' ) ); ?>"><?php _e( 'Number of posts to show:', 'news-essence' ); ?></label>
		<input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'number' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'number' ) ); ?>" type="text" value="<?php echo esc_attr( $number ); ?>" size="3" /></p>
		<p><label for="<?php echo esc_attr( $this->get_field_id( 'cat_id' ) ); ?>"><?php _e( 'Category:', 'news-essence' ); ?></label>
		<?php wp_dropdown_categories( 'name='.$this->get_field_name( 'cat_id' ).'&class=widefat&show_option_all=All&hide_empty=0&hierarchical=1&depth=2&selected='.$cat_id); ?></p>
		<?php
	}

	public function update($new_instance, $old_instance){
		$instance['title'] = strip_tags( $new_instance['title'] );
		$instance['number'] = (int) $new_instance['number'];
		$instance['cat_id'] = (int) $new_instance['cat_id'];
		return $instance;

	}

	public function widget($args, $instance) {

		$title = ( ! empty( $instance['title'] ) ) ? $instance['title'] : '';
		$title = apply_filters( 'widget_title', $title, $instance, $this->id_base );
		$number = ( ! empty( $instance['number'] ) ) ? absint( $instance['number'] ) : 5;
		$cat_id = ( ! empty( $instance['cat_id'] ) ) ? absint( $instance['cat_id'] ) : 0;

		$args = array(
			
			'orderby' 			=> 'date',
			'order'				=> 'DESC',
			'cat'				=>	$cat_id,
			'posts_per_page'	=> $number,
			'post_type'			=>'post',
			'no_found_rows' => true,
			'post_status' => 'publish',
			'ignore_sticky_posts' => true,
					
		);

		$catpost = new WP_Query($args);

		$s1 = array(1,2,9,10,17,18,25,26,33,34,41,42,49,50,57,58,65,66,73,74,81,82,89,90,97,98);
		$s2 = array(3,4,5,11,12,13,19,20,21,27,28,29,35,36,37,43,44,45,51,52,53,59,60,61,67,68,69,75,76,77,83,84,85,91,92,93);
		$s21 = array(3,11,19,27,35,43,51,59,67,75,83,91);
		$s3 = array(6,7,8,14,15,16,22,23,24,30,31,32,38,39,40,46,47,48,54,55,56,62,63,64,70,71,72,78,79,80,86,87,88,94,95,96);
		$s31 =array(6,14,22,30,38,46,54,62,70,78,86,94);

		  	?>
		  	<h3 class="cat-title"><?php echo $title; ?> </h3>
		  	<?php $i = 1; while ( $catpost->have_posts() ) : $catpost->the_post(); 
		    
		    	if (in_array($i, $s1)){ ?>
		    		<div class="col-md-6 col-sm-6">
			    		<div class="panel panel-default box-shadow--2dp news-cell--big">
			    		  		<?php echo '<a class="news-img img-responsive" data-block="3" data-position="4" style="background-image:url('.wp_get_attachment_url( get_post_thumbnail_id() ).');" href='.get_permalink().'></a>'; ?>
	  					  		<div class="panel-body news_body">
	  					  			<p class="news_title">
	  					  		    <a data-block="4" data-position="4" href="<?php the_permalink(); ?>"><?php the_title();?></a>
	  					  		    </p>
	  					  		  </div>
	 					  <div class="panel-footer footer-social">
		 					  	<ul class="nav info-social">
		 					  	<li class="item-info news-time"><i class="fa fa-calendar-o" aria-hidden="true"> <?php echo human_time_diff( get_the_time( 'U' ), current_time('timestamp') ); ?> ago</i>
		 					  	</li>
		 					  	<li class="item-info info-views"><i class="fa fa-eye" aria-hidden="true"> <?php echo wpb_get_post_views(get_the_ID()); ?> </i>
		 					  	</li>
		 					  	
		 					  	<li class="item-info info-comment"><i class="fa fa-comment" aria-hidden="true"></i><?php echo get_comments_number(); ?>
		 					  	</li>
		 					    </ul>
		 				  </div>
						</div>
		    		</div>

		    	<?php } 

		    	if (in_array($i, $s2))
		    	{ 

				    if($i % 2 == 1) 
				    {

				    	if(in_array($i, $s21)) {?>
							 		<div class="col-md-6 col-sm-6">
				    		 		<div class="panel panel-default box-shadow--2dp news-cell--small">
					    		 <div class="panel-body news_body">
	  					  			<p class="news_title">
	  					  		    <a data-block="4" data-position="4" href="<?php the_permalink(); ?>"><?php the_title();?></a>
	  					  		    </p>

	  					  			</div>
							<div class="panel-footer footer-social">
		 					  	<ul class="nav info-social">
		 					  	<li class="item-info news-time"><i class="fa fa-calendar-o" aria-hidden="true"> <?php echo human_time_diff( get_the_time( 'U' ), current_time('timestamp') ); ?> ago</i>
		 					  	</li>
		 					  	<li class="item-info info-views"><i class="fa fa-eye" aria-hidden="true"> <?php echo wpb_get_post_views(get_the_ID()); ?> </i>
		 					  	</li>
		 					  	
		 					  	<li class="item-info info-comment"><i class="fa fa-comment" aria-hidden="true"></i><?php echo get_comments_number(); ?>
		 					  	</li>
		 					    </ul>
		 				  </div>
							 		</div>
							 	<?php if (in_array($number, $s21)):?> </div> <?php endif; ?>
						<?php } else { ?>
								<div class="col-md-6 col-sm-6"> 
								<div class="panel panel-default box-shadow--2dp news-cell--big">
			    		  		<?php echo '<a class="news-img img-responsive" data-block="3" data-position="4" style="background-image:url('.wp_get_attachment_url( get_post_thumbnail_id() ).');" href='.get_permalink().'></a>'; ?>
	  					  		<div class="panel-body news_body">
	  					  			<p class="news_title">
	  					  		    <a data-block="4" data-position="4" href="<?php the_permalink(); ?>"><?php the_title();?></a>
	  					  		    </p>
	  					  			</div>
	 					      <div class="panel-footer footer-social">
		 					  	<ul class="nav info-social">
		 					  	<li class="item-info news-time"><i class="fa fa-calendar-o" aria-hidden="true"> <?php echo human_time_diff( get_the_time( 'U' ), current_time('timestamp') ); ?> ago</i>
		 					  	</li>
		 					  	<li class="item-info info-views"><i class="fa fa-eye" aria-hidden="true"> <?php echo wpb_get_post_views(get_the_ID()); ?> </i>
		 					  	</li>
		 					  	
		 					  	<li class="item-info info-comment"><i class="fa fa-comment" aria-hidden="true"></i><?php echo get_comments_number(); ?>
		 					  	</li>
		 					    </ul>
		 				  </div>
		 					       
						        </div>
							    </div>
						<?php } 
					}
						else { ?>

								<div class="panel panel-default box-shadow--2dp news-cell--small">
					    		 <div class="panel-body news_body">
	  					  			<p class="news_title">
	  					  		    <a data-block="4" data-position="4" href="<?php the_permalink(); ?>"><?php the_title();?></a>
	  					  		    </p>
	  				
	  					  			</div>
			 				<div class="panel-footer footer-social">
		 					  	<ul class="nav info-social">
		 					  	<li class="item-info news-time"><i class="fa fa-calendar-o" aria-hidden="true"> <?php echo human_time_diff( get_the_time( 'U' ), current_time('timestamp') ); ?> ago</i>
		 					  	</li>
		 					  	<li class="item-info info-views"><i class="fa fa-eye" aria-hidden="true"> <?php echo wpb_get_post_views(get_the_ID()); ?> </i>
		 					  	</li>
		 					  	
		 					  	<li class="item-info info-comment"><i class="fa fa-comment" aria-hidden="true"></i><?php echo get_comments_number(); ?>
		 					  	</li>
		 					    </ul>
		 				  </div>
								</div>
								</div>
						<?php } 

				}?>

				    			
				<?php	if (in_array($i, $s3)){

							if (in_array($i, $s31)){ ?>

								<div class="col-md-6 col-sm-6">
		    					<div class="panel panel-default box-shadow--2dp news-cell--big">
			    		  		<?php echo '<a class="news-img img-responsive" data-block="3" data-position="4" style="background-image:url('.wp_get_attachment_url( get_post_thumbnail_id() ).');" href='.get_permalink().'></a>'; ?>
	  					  		<div class="panel-body news_body">
	  					  			<p class="news_title">
	  					  		    <a data-block="4" data-position="4" href="<?php the_permalink(); ?>"><?php the_title();?></a>
	  					  		    </p>
	  					  		   
	  					  			</div>
	 					  	<div class="panel-footer footer-social">
		 					  	<ul class="nav info-social">
		 					  	<li class="item-info news-time"><i class="fa fa-calendar-o" aria-hidden="true"> <?php echo human_time_diff( get_the_time( 'U' ), current_time('timestamp') ); ?> ago</i>
		 					  	</li>
		 					  	<li class="item-info info-views"><i class="fa fa-eye" aria-hidden="true"> <?php echo wpb_get_post_views(get_the_ID()); ?> </i>
		 					  	</li>
		 					  	
		 					  	<li class="item-info info-comment"><i class="fa fa-comment" aria-hidden="true"></i><?php echo get_comments_number(); ?>
		 					  	</li>
		 					    </ul>
		 				  </div>
								</div>
								</div>

						<?php	} else {


								if($i % 2 == 1) { ?>

									<div class="col-md-6 col-sm-6">
				    		 		<div class="panel panel-default box-shadow--2dp news-cell--small">
					    		 <div class="panel-body news_body">
	  					  			<p class="news_title">
	  					  		    <a data-block="4" data-position="4" href="<?php the_permalink(); ?>"><?php the_title();?></a>
	  					  		    </p>
	  					  			</div>
				 			<div class="panel-footer footer-social">
		 					  	<ul class="nav info-social">
		 					  	<li class="item-info news-time"><i class="fa fa-calendar-o" aria-hidden="true"> <?php echo human_time_diff( get_the_time( 'U' ), current_time('timestamp') ); ?> ago</i>
		 					  	</li>
		 					  	<li class="item-info info-views"><i class="fa fa-eye" aria-hidden="true"> <?php echo wpb_get_post_views(get_the_ID()); ?> </i>
		 					  	</li>
		 					  	
		 					  	<li class="item-info info-comment"><i class="fa fa-comment" aria-hidden="true"></i><?php echo get_comments_number(); ?>
		 					  	</li>
		 					    </ul>
		 				  </div>
							 		</div>
							 	<?php if ( $i == $number):?> </div> <?php endif; ?>

								<?php } else { ?>

									<div class="panel panel-default box-shadow--2dp news-cell--small">
							    		 <div class="panel-body news_body">
			  					  			<p class="news_title">
			  					  		    <a data-block="4" data-position="4" href="<?php the_permalink(); ?>"><?php the_title();?></a>
			  					  		    </p>
			  					  		    
			  					  			</div>
			 				<div class="panel-footer footer-social">
		 					  	<ul class="nav info-social">
		 					  	<li class="item-info news-time"><i class="fa fa-calendar-o" aria-hidden="true"> <?php echo human_time_diff( get_the_time( 'U' ), current_time('timestamp') ); ?> ago</i>
		 					  	</li>
		 					  	<li class="item-info info-views"><i class="fa fa-eye" aria-hidden="true"> <?php echo wpb_get_post_views(get_the_ID()); ?> </i>
		 					  	</li>
		 					  	
		 					  	<li class="item-info info-comment"><i class="fa fa-comment" aria-hidden="true"></i><?php echo get_comments_number(); ?>
		 					  	</li>
		 					    </ul>
		 				  </div>
								</div>
								</div>

						<?php		}



						}
					}
		    			
		    $i++; endwhile;	
			wp_reset_postdata();

	 } 
}
?>