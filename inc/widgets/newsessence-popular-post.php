<?php
/**
* NewsEssence Trending Post widget based on views
*/


function register_popular_content_widget(){

	register_widget('newsessence_popular_content_widget');
}

add_action('widgets_init', 'register_popular_content_widget');

class newsessence_popular_content_widget extends WP_Widget {
	
	public function __construct()
	{
		
		parent::__construct(
				'newsessence_popular_widget',
				_('NewsEssence - Popular Post'),
				 array('description' =>_('Display popular posts based on views or comments'),)
		);
	}

	public function form($instance){
		
		$title = isset( $instance['title'] ) ? esc_attr( $instance['title'] ) : '';
		$number = isset( $instance['number'] ) ? absint( $instance['number'] ) : 5;
		$period = isset( $instance['period'] ) ? esc_textarea( $instance['period'] ):7;
		$order_by = isset($instance['order_by']) ?  esc_textarea( $instance['order_by'] ):'views';

		?>

		<p><label for="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>"><?php _e( 'Title:', 'news-essence' ); ?></label>
		<input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'title' ) ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>" /></p>
		<p><label for="<?php echo esc_attr( $this->get_field_id( 'number' ) ); ?>"><?php _e( 'Number of posts to show:', 'news-essence' ); ?></label>
		<input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'number' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'number' ) ); ?>" type="text" value="<?php echo esc_attr( $number ); ?>" size="3" /></p>
		<p>
            <label for="<?php echo $this->get_field_id( 'period' ); ?>"><?php _e( 'Post period', 'news-essence' ); ?></label>
            <select class="widefat" id="<?php echo $this->get_field_id( 'period' ); ?>" name="<?php echo $this->get_field_name( 'period' ); ?>">
                <option value="1" <?php if( $period === '1' ) echo 'selected'; ?>><?php _e('Today','news-essence'); ?></option>
                <option value="7" <?php if( $period === '7' ) echo 'selected'; ?>><?php _e('1 Week','news-essence'); ?></option>
                <option value="30" <?php if( $period === '30' ) echo 'selected'; ?>><?php _e('1 Month','news-essence'); ?></option>
                <option value="120" <?php if( $period === '120' ) echo 'selected'; ?>><?php _e('4 Months','news-essence'); ?></option>
                <option value="180" <?php if( $period === '180' ) echo 'selected'; ?>><?php _e('6 Months','news-essence'); ?></option>
                <option value="365" <?php if( $period === '365' ) echo 'selected'; ?>><?php _e('1 Year','news-essence'); ?></option>
                <option value="3650" <?php if( $period === '3650' ) echo 'selected'; ?>><?php _e('over a Year','news-essence'); ?></option>
            </select>
        </p>
        <p>
	<label for="<?php echo $this->get_field_id( 'order_by' ); ?>"><?php _e('Sort posts by', 'news-essence'); ?>:</label><br />
    <select id="<?php echo $this->get_field_id( 'order_by' ); ?>" name="<?php echo $this->get_field_name( 'order_by' ); ?>" class="widefat">
        <option value="comments" <?php if ( 'comments' == $order_by ) echo 'selected'; ?>><?php _e('Comments', 'news-essence'); ?></option>
        <option value="views" <?php if ( 'views' == $order_by ) echo 'selected'; ?>><?php _e('Views', 'news-essence'); ?></option>
        <option value="random" <?php if ( 'random' == $order_by ) echo 'selected'; ?>><?php _e('Random', 'news-essence'); ?></option>
    </select>
</p>

		<?php
	}

	public function update($new_instance, $old_instance){
		$instance['title'] = strip_tags( $new_instance['title'] );
		$instance['number'] = (int) $new_instance['number'];
	    $instance['period'] = strip_tags( $new_instance['period'] );
	    $instance['order_by'] = $new_instance['order_by'];
		return $instance;

	}

	public function widget($args, $instance) {

		$title = ( ! empty( $instance['title'] ) ) ? $instance['title'] : '';
		$title = apply_filters( 'widget_title', $title, $instance, $this->id_base );
		
		$number = ( ! empty( $instance['number'] ) ) ? absint( $instance['number'] ) : 0;
		$period = $instance['period'];
		$order_by = $instance['order_by'];

		if ($order_by == 'comments'){

			$args = array(
			
			'posts_per_page'	=> $number,
			'orderby' => 'comment_count',
			'order' => 'DESC',
			'post_type'			=>'post',
			'post_status' => 'publish',
			'ignore_sticky_posts' => true,
			'date_query' => array(
                array(
                    'after' => $period . ' days ago', // or '-7 days'
                    'inclusive' => true,
                ),
                ),
					
			);
		}
		elseif ($order_by == 'random') {
			# code...
			$args = array(
			
			'posts_per_page'	=> $number,
			'orderby' => 'rand',
			'order' => 'DESC',
			'post_type'			=>'post',
			'post_status' => 'publish',
			'ignore_sticky_posts' => true,
			'date_query' => array(
                array(
                    'after' => $period . ' days ago', // or '-7 days'
                    'inclusive' => true,
                ),
                ),
					
			);
		}
		else {

			$args = array(
			
			'posts_per_page'	=> $number,
			'meta_key' => 'wpb_post_views_count',
			'orderby' => 'meta_value_num',
			'order' => 'DESC',
			'post_type'			=>'post',
			'post_status' => 'publish',
			'ignore_sticky_posts' => true,
			'date_query' => array(
                array(
                    'after' => $period . ' days ago', // or '-7 days'
                    'inclusive' => true,
                ),
                ),
					
			);

		}
		

		$popularpost = new WP_Query($args);
		$s1 = array(1,2,9,10,17,18,25,26,33,34,41,42,49,50,57,58,65,66,73,74,81,82,89,90,97,98);
		$s2 = array(3,4,5,11,12,13,19,20,21,27,28,29,35,36,37,43,44,45,51,52,53,59,60,61,67,68,69,75,76,77,83,84,85,91,92,93);
		$s21 = array(3,11,19,27,35,43,51,59,67,75,83,91);
		$s3 = array(6,7,8,14,15,16,22,23,24,30,31,32,38,39,40,46,47,48,54,55,56,62,63,64,70,71,72,78,79,80,86,87,88,94,95,96);
		$s31 =array(6,14,22,30,38,46,54,62,70,78,86,94);

		  	?>
		  	<h3 class="cat-title"><?php echo $title; ?> </h3>
		  	<?php $i = 1; while ( $popularpost->have_posts() ) : $popularpost->the_post(); 
		    
		    	if (in_array($i, $s1)){ ?>
		    		<div class="col-md-6 col-sm-6">
			    		<div class="panel panel-default box-shadow--2dp news-cell--big">
			    		<?php 
			    		echo '<a class="news-img img-responsive" data-block="3" data-position="4" style="background-image:url('.wp_get_attachment_url( get_post_thumbnail_id() ).');" href='.get_permalink().'></a>'; ?>
	  					  <div class="panel-body news_body">
	  					  			<p class="news_title">
	  					  		    <a data-block="4" data-position="4" href="<?php the_permalink(); ?>"><?php the_title();?></a>
	  					  		    </p>
	  					  			</div>
	 					   <div class="panel-footer footer-social">
		 					  	<ul class="nav info-social">
		 					  	<li class="item-info news-time"><i class="fa fa-calendar-o" aria-hidden="true"> <?php echo human_time_diff( get_the_time( 'U' ), current_time('timestamp') ); ?> ago</i>
		 					  	</li>
		 					  	<li class="item-info info-views"><i class="fa fa-eye" aria-hidden="true"> <?php echo wpb_get_post_views(get_the_ID()); ?> </i>
		 					  	</li>
		 					  	
		 					  	<li class="item-info info-comment"><i class="fa fa-comment" aria-hidden="true"></i><?php echo get_comments_number(); ?>
		 					  	</li>
		 					    </ul>
		 				  </div>
						</div>
		    		</div>

		    	<?php } 

		    	if (in_array($i, $s2))
		    	{ 

				    if($i % 2 == 1) 
				    {

				    	if(in_array($i, $s21)) {?>
							 		<div class="col-md-6 col-sm-6">
				    		 		<div class="panel panel-default box-shadow--2dp news-cell--small">
						    		 <div class="panel-body news_body">
	  					  			<p class="news_title">
	  					  		    <a data-block="4" data-position="4" href="<?php the_permalink(); ?>"><?php the_title();?></a>
	  					  		    </p>
	  					  			</div>
				 			 <div class="panel-footer footer-social">
		 					  	<ul class="nav info-social">
		 					  	<li class="item-info news-time"><i class="fa fa-calendar-o" aria-hidden="true"> <?php echo human_time_diff( get_the_time( 'U' ), current_time('timestamp') ); ?> ago</i>
		 					  	</li>
		 					  	<li class="item-info info-views"><i class="fa fa-eye" aria-hidden="true"> <?php echo wpb_get_post_views(get_the_ID()); ?> </i>
		 					  	</li>
		 					  	
		 					  	<li class="item-info info-comment"><i class="fa fa-comment" aria-hidden="true"></i><?php echo get_comments_number(); ?>
		 					  	</li>
		 					    </ul>
		 				  </div>
							 		</div>
							 	<?php if (in_array($number, $s21)):?> </div> <?php endif; ?>
						<?php } else { ?>
								<div class="col-md-6 col-sm-6"> 
								<div class="panel panel-default box-shadow--2dp news-cell--big">
			    		        <?php echo '<a class="news-img img-responsive" data-block="3" data-position="4" style="background-image:url('.wp_get_attachment_url( get_post_thumbnail_id() ).');" href='.get_permalink().'></a>'; ?>
	  					        <div class="panel-body news_body">
	  					  			<p class="news_title">
	  					  		    <a data-block="4" data-position="4" href="<?php the_permalink(); ?>"><?php the_title();?></a>
	  					  		    </p>
	  					  			</div>
	 					        <div class="panel-footer footer-social">
		 					  	<ul class="nav info-social">
		 					  	<li class="item-info news-time"><i class="fa fa-calendar-o" aria-hidden="true"> <?php echo human_time_diff( get_the_time( 'U' ), current_time('timestamp') ); ?> ago</i>
		 					  	</li>
		 					  	<li class="item-info info-views"><i class="fa fa-eye" aria-hidden="true"> <?php echo wpb_get_post_views(get_the_ID()); ?> </i>
		 					  	</li>
		 					  	
		 					  	<li class="item-info info-comment"><i class="fa fa-comment" aria-hidden="true"></i><?php echo get_comments_number(); ?>
		 					  	</li>
		 					    </ul>
		 				  </div>
						        </div>
							    </div>
						<?php } 
					}
						else { ?>

								<div class="panel panel-default box-shadow--2dp news-cell--small">
					    		  <div class="panel-body news_body">
	  					  			<p class="news_title">
	  					  		    <a data-block="4" data-position="4" href="<?php the_permalink(); ?>"><?php the_title();?></a>
	  					  		    </p>
	  					  			</div>
							 <div class="panel-footer footer-social">
		 					  	<ul class="nav info-social">
		 					  	<li class="item-info news-time"><i class="fa fa-calendar-o" aria-hidden="true"> <?php echo human_time_diff( get_the_time( 'U' ), current_time('timestamp') ); ?> ago</i>
		 					  	</li>
		 					  	<li class="item-info info-views"><i class="fa fa-eye" aria-hidden="true"> <?php echo wpb_get_post_views(get_the_ID()); ?> </i>
		 					  	</li>
		 					  
		 					  	<li class="item-info info-comment"><i class="fa fa-comment" aria-hidden="true"></i><?php echo get_comments_number(); ?>
		 					  	</li>
		 					    </ul>
		 				  </div>
								</div>
								</div>
						<?php } 

				}?>

				    			
				<?php	if (in_array($i, $s3)){

							if (in_array($i, $s31)){ ?>

								<div class="col-md-6 col-sm-6">
		    					<div class="panel panel-default box-shadow--2dp news-cell--big">
			    		  		<?php echo '<a class="news-img img-responsive" data-block="3" data-position="4" style="background-image:url('.wp_get_attachment_url( get_post_thumbnail_id() ).');" href='.get_permalink().'></a>'; ?>
	  					
	  					  		    <div class="panel-body news_body">
	  					  			<p class="news_title">
	  					  		    <a data-block="4" data-position="4" href="<?php the_permalink(); ?>"><?php the_title();?></a>
	  					  		    </p>
	  					  			</div>
	 					   <div class="panel-footer footer-social">
		 					  	<ul class="nav info-social">
		 					  	<li class="item-info news-time"><i class="fa fa-calendar-o" aria-hidden="true"> <?php echo human_time_diff( get_the_time( 'U' ), current_time('timestamp') ); ?> ago</i>
		 					  	</li>
		 					  	<li class="item-info info-views"><i class="fa fa-eye" aria-hidden="true"> <?php echo wpb_get_post_views(get_the_ID()); ?> </i>
		 					  	</li>
		 					  	
		 					  	<li class="item-info info-comment"><i class="fa fa-comment" aria-hidden="true"></i><?php echo get_comments_number(); ?>
		 					  	</li>
		 					    </ul>
		 				  </div>
								</div>
								</div>

						<?php	} else {


								if($i % 2 == 1) { ?>

							<div class="col-md-6 col-sm-6">
				    		 		<div class="panel panel-default box-shadow--2dp news-cell--small">
						    		<div class="panel-body news_body">
	  					  			<p class="news_title">
	  					  		    <a data-block="4" data-position="4" href="<?php the_permalink(); ?>"><?php the_title();?></a>
	  					  		    </p>
	  					  			</div>
				 		 <div class="panel-footer footer-social">
		 					  	<ul class="nav info-social">
		 					  	<li class="item-info news-time"><i class="fa fa-calendar-o" aria-hidden="true"> <?php echo human_time_diff( get_the_time( 'U' ), current_time('timestamp') ); ?> ago</i>
		 					  	</li>
		 					  	<li class="item-info info-views"><i class="fa fa-eye" aria-hidden="true"> <?php echo wpb_get_post_views(get_the_ID()); ?> </i>
		 					  	</li>
		 					  	
		 					  	<li class="item-info info-comment"><i class="fa fa-comment" aria-hidden="true"></i><?php echo get_comments_number(); ?>
		 					  	</li>
		 					    </ul>
		 				  </div>
								</div>
							 	<?php if ( $i == $number):?> </div> <?php endif; ?>

								<?php } else { ?>

								    <div class="panel panel-default box-shadow--2dp news-cell--small">
					    				 <div class="panel-body news_body">
	  					  			<p class="news_title">
	  					  		    <a data-block="4" data-position="4" href="<?php the_permalink(); ?>"><?php the_title();?></a>
	  					  		    </p>
	  					  			</div>
			 				 <div class="panel-footer footer-social">
		 					  	<ul class="nav info-social">
		 					  	<li class="item-info news-time"><i class="fa fa-calendar-o" aria-hidden="true"> <?php echo human_time_diff( get_the_time( 'U' ), current_time('timestamp') ); ?> ago</i>
		 					  	</li>
		 					  	<li class="item-info info-views"><i class="fa fa-eye" aria-hidden="true"> <?php echo wpb_get_post_views(get_the_ID()); ?> </i>
		 					  	</li>
		 					  	
		 					  	<li class="item-info info-comment"><i class="fa fa-comment" aria-hidden="true"></i><?php echo get_comments_number(); ?>
		 					  	</li>
		 					    </ul>
		 				  </div>
								</div>
								</div>

						<?php		}



						}
					}
		    $i++; endwhile;	
			wp_reset_postdata();
	 } 
}