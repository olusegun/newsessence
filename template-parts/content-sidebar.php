<?php
/**
 * Template part for displaying posts.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package news_essence
 */

?>

<article itemscope="itemscope" itemtype="http://schema.org/BlogPosting" itemprop="blogPost" class="post-wrapper-sidebar" id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	
	<header class="entry-header">
		<?php
			//recommned to use wp-subtitle on post for better seo performance by installing wp_subtitle
			// check if wp_subtitle plugin is installed so as to use the 'the_subtitle' function
					
			
			if ( is_single() ) {

			 	the_title( '<h1 itemprop="headline" class="single-title">', '</h1>' );
			 		  
				
				
			} else {
					the_title( '<h2 itemprop="headline" class="entry-title"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h2>' );
					}

		if ( 'post' === get_post_type() ) : ?>
		<div class="entry-meta">
			<?php news_essence_posted_on(); ?>
			 <span class="fa fa-eye"> <?php echo wpb_get_post_views(get_the_ID()); ?> </span>
			 </div><!-- .entry-meta -->
		<?php
		endif; ?>
	</header><!-- .entry-header -->

	<div class="entry-content" itemprop="text">
		<?php
		
			the_content( sprintf(
				/* translators: %s: Name of current post. */
				the_post_thumbnail(),
				wp_kses( __( 'Continue reading %s <span class="meta-nav">&rarr;</span>', 'news-essence' ), array( 'span' => array( 'class' => array() ) ) ),
				the_title( '<span class="screen-reader-text">"', '"</span>', false )
			) );

			$args = array(
							'before' => '<ul class="pagination">',
							'after' => '</ul>',
							'before_link' => '<li>',
							'after_link' => '</li>',
							'current_before' => '<li class="active">',
							'current_after' => '</li>',
							'previouspagelink' => '&laquo;',
							'nextpagelink' => '&raquo;'
						);
			essence_link_pages( $args );

		?>
	</div><!-- .entry-content -->
	

	<footer class="entry-footer">
		<?php news_essence_entry_footer(); ?>
	</footer><!-- .entry-footer -->
</article><!-- #post-## -->