<?php
/**
 * Template part for displaying page content in page.php.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package news_essence
 */

?>

<article itemscope="itemscope" itemtype="http://schema.org/BlogPosting" itemprop="blogPost" id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<header class="entry-header">
		<?php the_title( '<h1 itemprop="headline" class="entry-title">', '</h1>' ); ?>
	</header><!-- .entry-header -->

	<div class="entry-content" itemprop="text">
		<?php
			the_content();

			$args = array(
							'before' => '<ul class="pagination">',
							'after' => '</ul>',
							'before_link' => '<li>',
							'after_link' => '</li>',
							'current_before' => '<li class="active">',
							'current_after' => '</li>',
							'previouspagelink' => '&laquo;',
							'nextpagelink' => '&raquo;'
						);
			essence_link_pages( $args );

		?>
	</div><!-- .entry-content -->

	<footer class="entry-footer">
		<?php
			edit_post_link(
				sprintf(
					/* translators: %s: Name of current post */
					esc_html__( 'Edit %s', 'news-essence' ),
					the_title( '<span class="screen-reader-text">"', '"</span>', false )
				),
				'<span class="edit-link">',
				'</span>'
			);
		?>
	</footer><!-- .entry-footer -->
</article><!-- #post-## -->
