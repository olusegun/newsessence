<?php
/**
 * The template for displaying search results pages.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#search-result
 *
 * @package news_essence
 */

get_header(); ?>
	<?php the_breadcrumb(); ?>
		<div class="col-sm-12 col-md-12 site-main" itemprop="mainContentOfPage" itemscope="itemscope" itemtype="http://schema.org/Blog">
		<?php
		if ( have_posts() ) :
			 $number = get_option('posts_per_page'); // get the default number of post from Reading Settings
				$s1 = array(1,2,3,12,13,14,17,18,23,24,25,34,35,36,45,46,47,56,57,58,67,68,69,78,79,80,89,90,91);
				$s2 = array(4,5,6,7,15,16,17,18,26,27,28,29,37,38,39,40,48,49,50,51,59,60,61,62,70,71,72,73,81,82,83,84,92,93,94,95);
				$s21 = array(4,15,26,37,48,59,70,81,92);
				$s3 = array(8,9,10,11,19,20,21,22,30,31,32,33,41,42,43,44,52,53,54,55,63,64,65,66,74,75,76,77,85,86,87,88,96,97,98,99);
				$s31 = array(9,20,31,42,53,64,75,86,97); ?>
			<header class="page-header">
				<h1 class="page-title"><?php printf( esc_html__( 'Search Results for: %s', 'news-essence' ), '<span>' . get_search_query() . '</span>' ); ?></h1>
			</header><!-- .page-header -->

			<?php
			/* Start the Loop */
			 $i=1; while ( have_posts() ) : the_post();

					if (in_array($i, $s1)){ ?>
		            <article class="col-md-4 col-sm-4"  id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
			    		<div class="panel panel-default box-shadow--2dp news-cell--big">
			    		  		<?php echo '<a class="news-img img-responsive" data-block="3" data-position="4" style="background-image:url('.wp_get_attachment_url( get_post_thumbnail_id() ).');" href='.get_permalink().'></a>'; ?>
	  					  	<div class="panel-body news_body">
			  					  		<?php the_title( '<h2 class="news_title entry-title"><a data-block="4" data-position="4" href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h2>' );?>
			  				</div>
	 					    <div class="panel-footer footer-social">
		 					  	<ul class="nav info-social">
		 					  	<li class="item-info news-time"><i class="fa fa-calendar-o" aria-hidden="true"> <?php echo human_time_diff( get_the_time( 'U' ), current_time('timestamp') ); ?> ago</i>
		 					  	</li>
		 					  	<li class="item-info info-views"><i class="fa fa-eye" aria-hidden="true"> <?php echo wpb_get_post_views(get_the_ID()); ?> </i>
		 					  	</li>
		 					  	 
		 					  	<li class="item-info info-comment"><i class="fa fa-comment" aria-hidden="true"></i><?php echo get_comments_number(); ?>
		 					  	</li>
		 					    </ul>
		 				  </div>
						</div>
		            </article>

		    	<?php } 

		    	if (in_array($i, $s2))
		    	{ 

				   	if($i % 11 == 4) {?>
						<article class="col-md-4 col-sm-4"  id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
				    		 	<div class="panel panel-default box-shadow--2dp news-cell--small">
					    		 <div class="panel-body news_body">
			  					  		<?php the_title( '<h2 class="news_title entry-title"><a data-block="4" data-position="4" href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h2>' );?>
			  					</div>
							<div class="panel-footer footer-social">
		 					  	<ul class="nav info-social">
		 					  	<li class="item-info news-time"><i class="fa fa-calendar-o" aria-hidden="true"> <?php echo human_time_diff( get_the_time( 'U' ), current_time('timestamp') ); ?> ago</i>
		 					  	</li>
		 					  	<li class="item-info info-views"><i class="fa fa-eye" aria-hidden="true"> <?php echo wpb_get_post_views(get_the_ID()); ?> </i>
		 					  	</li>
		 					  	 
		 					  	<li class="item-info info-comment"><i class="fa fa-comment" aria-hidden="true"></i><?php echo get_comments_number(); ?>
		 					  	</li>
		 					    </ul>
		 				  </div>
							 		</div>
							 	<?php if (in_array($number, $s21)):?> </article> <?php endif; ?>
						<?php } 

					if ($i % 11 == 5) { ?>

								<div class="panel panel-default box-shadow--2dp news-cell--small">
					    		 <div class="panel-body news_body">
			  					  		<?php the_title( '<h2 class="news_title entry-title"><a data-block="4" data-position="4" href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h2>' );?>
			  				</div>
			 				<div class="panel-footer footer-social">
		 					  	<ul class="nav info-social">
		 					  	<li class="item-info news-time"><i class="fa fa-calendar-o" aria-hidden="true"> <?php echo human_time_diff( get_the_time( 'U' ), current_time('timestamp') ); ?> ago</i>
		 					  	</li>
		 					  	<li class="item-info info-views"><i class="fa fa-eye" aria-hidden="true"> <?php echo wpb_get_post_views(get_the_ID()); ?> </i>
		 					  	</li>
		 					  	 
		 					  	<li class="item-info info-comment"><i class="fa fa-comment" aria-hidden="true"></i><?php echo get_comments_number(); ?>
		 					  	</li>
		 					    </ul>
		 				  </div>
								</div>
								</article>

					<?php }

						if ( ($i % 11 == 6)  || ($i % 11== 7)){ ?>

							<article class="col-md-4 col-sm-4"  id="post-<?php the_ID(); ?>" <?php post_class(); ?>> 
								<div class="panel panel-default box-shadow--2dp news-cell--big">
			    		  		<?php echo '<a class="news-img img-responsive" data-block="3" data-position="4" style="background-image:url('.wp_get_attachment_url( get_post_thumbnail_id() ).');" href='.get_permalink().'></a>'; ?>
	  					  		<div class="panel-body news_body">
			  					  		<?php the_title( '<h2 class="news_title entry-title"><a data-block="4" data-position="4" href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h2>' );?>
			  				</div>
	 					    <div class="panel-footer footer-social">
		 					  	<ul class="nav info-social">
		 					  	<li class="item-info news-time"><i class="fa fa-calendar-o" aria-hidden="true"> <?php echo human_time_diff( get_the_time( 'U' ), current_time('timestamp') ); ?> ago</i>
		 					  	</li>
		 					  	<li class="item-info info-views"><i class="fa fa-eye" aria-hidden="true"> <?php echo wpb_get_post_views(get_the_ID()); ?> </i>
		 					  	</li>
		 					  	 
		 					  	<li class="item-info info-comment"><i class="fa fa-comment" aria-hidden="true"></i><?php echo get_comments_number(); ?>
		 					  	</li>
		 					    </ul>
		 				  </div>
		 				</div>
						</article>

						<?php  }
							
				} 
				
					if (in_array($i, $s3)){

							if ( $i % 11 == 8 || $i % 11 == 0){ ?>

							<article class="col-md-4 col-sm-4"  id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
		    					<div class="panel panel-default box-shadow--2dp news-cell--big">
			    		  		<?php echo '<a class="news-img img-responsive" data-block="3" data-position="4" style="background-image:url('.wp_get_attachment_url( get_post_thumbnail_id() ).');" href='.get_permalink().'></a>'; ?>
	  					  		<div class="panel-body news_body">
			  					  		<?php the_title( '<h2 class="news_title entry-title"><a data-block="4" data-position="4" href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h2>' );?>
			  				</div>
	 					  	<div class="panel-footer footer-social">
		 					  	<ul class="nav info-social">
		 					  	<li class="item-info news-time"><i class="fa fa-calendar-o" aria-hidden="true"> <?php echo human_time_diff( get_the_time( 'U' ), current_time('timestamp') ); ?> ago</i>
		 					  	</li>
		 					  	<li class="item-info info-views"><i class="fa fa-eye" aria-hidden="true"> <?php echo wpb_get_post_views(get_the_ID()); ?> </i>
		 					  	</li>
		 					  	 
		 					  	<li class="item-info info-comment"><i class="fa fa-comment" aria-hidden="true"></i><?php echo get_comments_number(); ?>
		 					  	</li>
		 					    </ul>
		 				  </div>
								</div>
							</article>

						<?php	} 

								if($i % 11 == 9 ) { ?>

								<article class="col-md-4 col-sm-4"  id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
				    		 		<div class="panel panel-default box-shadow--2dp news-cell--small">
					    		 	<div class="panel-body news_body">
			  					  		<?php the_title( '<h2 class="news_title entry-title"><a data-block="4" data-position="4" href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h2>' );?>
			  				</div>
	  					  	<div class="panel-footer footer-social">
		 					  	<ul class="nav info-social">
		 					  	<li class="item-info news-time"><i class="fa fa-calendar-o" aria-hidden="true"> <?php echo human_time_diff( get_the_time( 'U' ), current_time('timestamp') ); ?> ago</i>
		 					  	</li>
		 					  	<li class="item-info info-views"><i class="fa fa-eye" aria-hidden="true"> <?php echo wpb_get_post_views(get_the_ID()); ?> </i>
		 					  	</li>
		 					  	 
		 					  	<li class="item-info info-comment"><i class="fa fa-comment" aria-hidden="true"></i><?php echo get_comments_number(); ?>
		 					  	</li>
		 					    </ul>
		 				  </div>
							 		</div>
							 	<?php if (in_array($number, $s31)):?> </article> <?php endif; ?>

								<?php } 

								if($i % 11 == 10){ ?>

									<div class="panel panel-default box-shadow--2dp news-cell--small">
							    		<div class="panel-body news_body">
			  					  		<?php the_title( '<h2 class="news_title entry-title"><a data-block="4" data-position="4" href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h2>' );?>
			  				</div>
			 				<div class="panel-footer footer-social">
		 					  	<ul class="nav info-social">
		 					  	<li class="item-info news-time"><i class="fa fa-calendar-o" aria-hidden="true"> <?php echo human_time_diff( get_the_time( 'U' ), current_time('timestamp') ); ?> ago</i>
		 					  	</li>
		 					  	<li class="item-info info-views"><i class="fa fa-eye" aria-hidden="true"> <?php echo wpb_get_post_views(get_the_ID()); ?> </i>
		 					  	</li>
		 					  	 
		 					  	<li class="item-info info-comment"><i class="fa fa-comment" aria-hidden="true"></i><?php echo get_comments_number(); ?>
		 					  	</li>
		 					    </ul>
		 				  </div>
								</div>
								</article>

						<?php		}
						
					}

				$i++; endwhile;

				else :

					get_template_part( 'template-parts/content', 'none' );

				endif; 
				
				?>
		</div><!-- #main -->
		<?php
		if(function_exists('wp_pagenavi')){
					wp_pagenavi();
				}
				else {
					the_posts_navigation();
				}
				?>
		</div><!-- #content -->
<?php
get_footer();