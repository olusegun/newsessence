<?php
/**
 * news essence functions and definitions.
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package news_essence
 */

if ( ! function_exists( 'news_essence_setup' ) ) :
/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 */
function news_essence_setup() {
	/*
	 * Make theme available for translation.
	 * Translations can be filed in the /languages/ directory.
	 * If you're building a theme based on news essence, use a find and replace
	 * to change 'news-essence' to the name of your theme in all the template files.
	 */
	load_theme_textdomain( 'news-essence', get_template_directory() . '/languages' );

	// Add default posts and comments RSS feed links to head.
	add_theme_support( 'automatic-feed-links' );

	/*
	 * Let WordPress manage the document title.
	 * By adding theme support, we declare that this theme does not use a
	 * hard-coded <title> tag in the document head, and expect WordPress to
	 * provide it for us.
	 */
	add_theme_support( 'title-tag' );

	/*
	 * Enable support for Post Thumbnails on posts and pages.
	 *
	 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
	 */
	add_theme_support( 'post-thumbnails' );

	
		// Register Navigation Menus

		function custom_navigation_menus() {

			$locations = array(
				'primary' => __( 'main menu', 'news-essence' ),
				'secondary' => __( 'top menu', 'news-essence' )
			);
			register_nav_menus( $locations );

		}
		add_action( 'init', 'custom_navigation_menus' );


	/*
	 * Switch default core markup for search form, comment form, and comments
	 * to output valid HTML5.
	 */
	add_theme_support( 'html5', array(
		'search-form',
		'comment-form',
		'comment-list',
		'gallery',
		'caption',
	) );

	/*
	 * Enable support for Post Formats.
	 * See https://developer.wordpress.org/themes/functionality/post-formats/
	 */
	add_theme_support( 'post-formats', array(
		'gallery',
		'image',
		'video',
		'audio',
		'link',
	) );

	// Set up the WordPress core custom background feature.
	add_theme_support( 'custom-background', apply_filters( 'news_essence_custom_background_args', array(
		'default-color' => 'ffffff',
		'default-image' => '',
	) ) );
}
endif;
add_action( 'after_setup_theme', 'news_essence_setup' );


add_filter( 'get_the_archive_title', function ($title) {

    if ( is_category() ) {

            $title = single_cat_title( '', false );

        } elseif ( is_tag() ) {

            $title = single_tag_title( '', false );

        } elseif ( is_author() ) {

            $title = '<span class="vcard">' . get_the_author() . '</span>' ;

        }

    return $title;

});



/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function news_essence_content_width() {
	$GLOBALS['content_width'] = apply_filters( 'news_essence_content_width', 640 );
}
add_action( 'after_setup_theme', 'news_essence_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */

function news_essence_add_editor_styles(){
	add_editor_style('editor-style.css');

}
	add_action('admin_init', 'news_essence_add_editor_styles');

/**
 * Enqueue scripts and styles.
 */
function news_essence_scripts() {
	wp_enqueue_style( 'news-essence-style', get_stylesheet_uri() );

	wp_enqueue_script( 'jqueryjs', get_template_directory_uri() . '/js/jquery.min.js',array(),'20160524v1', true );
	wp_enqueue_script( 'bsjs', get_template_directory_uri() . '/js/bootstrap.min.js',array(),'2016052401', true );
	wp_enqueue_script( 'newstickerjs', get_template_directory_uri() . '/js/jquery.ticker.min.js',array(),'2016052401', true );
	wp_enqueue_script( 'mainjs', get_template_directory_uri() . '/js/main.js',array(),'2016052401', true);
    wp_enqueue_script( 'news-essence-navigation', get_template_directory_uri() . '/js/navigation.js', array(), '20151215', true );
	wp_enqueue_script( 'news-essence-skip-link-focus-fix', get_template_directory_uri() . '/js/skip-link-focus-fix.js', array(), '20151215', true );
	wp_enqueue_script( 'news-essence-ie10', get_template_directory_uri() . '/js/ie10-viewport-bug-workaround.js', array(), '20160524', true );
	
	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}

function news_essence_styles(){
  wp_enqueue_style( 'news-essence-style', get_stylesheet_uri() );
   wp_enqueue_style( 'base-style', get_template_directory_uri() . '/css/bootstrap.min.css');
   wp_enqueue_style( 'main-style', get_template_directory_uri() . '/css/main.css');
   wp_enqueue_style( 'pagenvi-style', get_template_directory_uri() . '/css/pagenavi-css.css');
   wp_enqueue_style( 'font-style', get_template_directory_uri() . '/fonts/css/font-awesome.min.css');
   wp_enqueue_style( 'font-style', get_template_directory_uri() . '/css/ie10-viewport-bug-workaround.css');
}



add_action( 'wp_enqueue_scripts', 'news_essence_scripts' );
add_action( 'wp_enqueue_scripts', 'news_essence_styles' );


/**
 * Implement post views.
 */
require get_template_directory() . '/inc/newsessence-postviews.php';

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Implement the Theme CSS option.
 */
require get_template_directory() . '/inc/custom-css.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Custom functions that act independently of the theme templates.
 */
require get_template_directory() . '/inc/extras.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';


/**
 * Load Jetpack compatibility file.
 */
require get_template_directory() . '/inc/jetpack.php';

/**
 * Walker Navbar.
 */
require_once get_template_directory(). '/inc/wp_bootstrap_navwalker.php';

/**
 * Register widget area
 */
require get_template_directory(). '/inc/newsessence-widget-area.php';
/**
 * Essence Widget function
 */
require get_template_directory(). '/inc/newsessence-widget.php';
/**
 * Essence-breadcrumb
 */
require get_template_directory(). '/inc/newsessence-breadcrumb.php';
/**
 * Theme-plugin-recommended
 */
require get_template_directory(). '/inc/theme-plugin.php';