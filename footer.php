<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package news_essence
 */

?>
	<footer id="colophon" class="site-footer" role="contentinfo" itemscope="itemscope" itemtype="http://schema.org/WPFooter">
		<div class="site-info" style="text-align:center;">
			<a href="<?php echo esc_url( __( 'https://wordpress.org/', 'news-essence' ) ); ?>"><?php printf( esc_html__( 'Proudly powered by %s', 'news-essence' ), 'WordPress' ); ?></a>
			<span class="sep"> | &copy; </span>
			<?php printf( esc_html__( '2016 newsessence theme developed %1$s by %2$s.', 'news-essence' ), '', '<a href="http://wptheme.smatecsystems.com/" rel="designer">SMAtec Team</a>' ); ?>
		</div><!-- .site-info -->
	</footer><!-- #colophon -->
</div><!-- #page -->

<?php wp_footer(); ?>
</body>
</html>
